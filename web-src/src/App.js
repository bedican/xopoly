import React, { Component } from 'react';
import io from 'socket.io-client'
import './App.css';

import Game from './components/game'
import GameList from './components/game-list'

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initData: null
        };
    }

    componentWillMount() {

        this.socket = io();

        this.socket.on('init', data => {

            this.setState({
                initData: data
            });

            localStorage.setItem('id', data.id);

            if(data.game) {
                this.attachThemeStylesheet(data.game.theme);
            }
        });

        this.socket.on('game-join', data => {
            this.socket.emit('init', {
                'id': localStorage.getItem('id') || false
            });
        });

        this.socket.on('game-create', data => {
            this.socket.emit('init', {
                'id': localStorage.getItem('id') || false
            });
        });

        this.socket.on('game-leave', data => {
            this.removeThemeStylesheet();
            this.socket.emit('init', {
                'id': localStorage.getItem('id') || false
            });
        });
    }

    componentDidMount() {
        this.socket.emit('init', {
            'id': localStorage.getItem('id') || false
        });
    }

    attachThemeStylesheet(href) {
        let theme = document.createElement('link');

        theme.id = 'game-theme';
        theme.rel = 'stylesheet';
        theme.type = 'text/css';
        theme.href = href;

        document.getElementsByTagName('head')[0].appendChild(theme);
    }

    removeThemeStylesheet() {
        document.getElementById('game-theme').remove();
    }

    render() {
        if (!this.state.initData) {
            return (<div className="App">Loading...</div>);
        }

        return (
            <div className="App">
                <main>
                    {this.state.initData.game ?
                        <Game socket={this.socket} initData={this.state.initData} /> :
                        <GameList socket={this.socket} initData={this.state.initData} />
                    }
                </main>
                <footer>&copy; Copyright 2018 <a href="http://www.bedican.co.uk">www.bedican.co.uk</a></footer>
            </div>
        );
    }
}

export default App;
