import React, { Component } from 'react';

import './index.css';

class GameBoard extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            player: null,
            players: {},
            isCurrentPlayer: false,
            trade: null,
            selectedPosition: false,
            playerPositions: {},
            deedPositions: {}
        };
    }

    componentWillMount() {
        this.fnPlayerState = (data) => { this.onPlayerState(data); };
        this.fnPlayerLeave = (data) => { this.onPlayerLeave(data); };
        this.fnPlayerList = (data) => { this.onPlayerList(data); };
        this.fnPlayState = (data) => { this.onPlayState(data); };

        this.socket.on('player-join', this.fnPlayerState);
        this.socket.on('player-state', this.fnPlayerState);
        this.socket.on('player-leave', this.fnPlayerLeave);
        this.socket.on('player-list', this.fnPlayerList);
        this.socket.on('play-state', this.fnPlayState);
    }

    componentWillUnmount() {
        this.socket.off('player-join', this.fnPlayerState);
        this.socket.off('player-state', this.fnPlayerState);
        this.socket.off('player-leave', this.fnPlayerLeave);
        this.socket.off('player-list', this.fnPlayerList);
        this.socket.off('play-state', this.fnPlayState);
    }

    componentDidMount() {
        if (this.initData.player) {
            this.onPlayerState(this.initData.player);
        }

        this.onPlayerList(this.initData.playerList);
        this.onPlayState(this.initData.playState);
    }

    getDeedByPosition(position) {
        for (var p in this.state.deedPositions) {
            if(this.state.deedPositions[p].position === position) {
                return this.state.deedPositions[p];
            }
        }

        return null;
    }

    getPlayersByPosition(position) {

        let players = [];

        for (var playerId in this.state.playerPositions) {
            if (this.state.playerPositions[playerId] === position) {
                if (this.state.players[playerId]) {
                    players.push(this.state.players[playerId]);
                }
            }
        }

        return players;
    }

    onPlayerState(player) {
        this.setState({
            player: player
        });
    }

    onPlayerLeave(data) {
        if (data.userId === this.initData.userId) {
            this.setState({
                player: null
            });
        }
    }

    onPlayerList(data) {

        let players = {};

        if (!data.players) {
            return;
        }

        for(var p in data.players) {
            players[data.players[p].id] = data.players[p];
        }

        this.setState({
            players: players,
        });
    }

    onPlayState(data) {
        this.setState({
            isCurrentPlayer: ((data.user) && (data.user === this.initData.userId)),
            trade: data.trade,
            playerPositions: data.playerPositions,
            deedPositions: data.deedPositions
        });
    }

    onClickPosition(position) {
        this.setState({
            selectedPosition: position
        });
    }

    onClickCurrentDeed(e) {
        this.setState({
            selectedPosition: false
        });
    }

    onClickTrade() {
        let deed = this.getDeedByPosition(this.state.selectedPosition);

        if (deed) {
            this.socket.emit('trade-deed', {
                id: deed.id
            });
        }
    }

    renderSelectedDeed(deed) {

        // can trade if following is met
        // deed is owned
        // trade does not exist or trade exists and has not yet been offered
        // trade does not exist or trade exists and deed is owned by either trading party or there is no other trading party
        // trade does not exist or trade exists and the deed is not currently in the trade
        // trade does not exist or trade exists and you are the trade player (owner)
        // trade exists or trade does not exist and you are the current player

        let canTrade =
            (deed.player) &&
            ((!this.state.trade) || (!this.state.trade.offered)) &&
            ((!this.state.trade) || (deed.player === this.state.trade.player) || (!this.state.trade.withPlayer) || ((this.state.trade.withPlayer) && (deed.player === this.state.trade.withPlayer))) &&
            ((!this.state.trade) || (!deed.inTrade)) &&
            ((!this.state.trade) || (this.state.trade.player === this.state.player.id)) &&
            ((this.state.trade) || (this.state.isCurrentPlayer));

        return (
            <div className="selected-deed">
                <div className="image">
                    <img onClick={this.onClickCurrentDeed.bind(this)} src={deed.image} alt={deed.name} />
                </div>
                <div className="details">
                    {deed.playerName && (<div>Owned by: {deed.playerName}</div>)}
                    <div>Price: {deed.price}</div>
                    <div>Rent: {deed.rental}</div>
                    <div>Mortgaged: {deed.mortgaged ? 'Yes' : 'No'}</div>
                    {deed.housePrice !== false && (<div>House Price: {deed.housePrice}</div>)}
                    {deed.housePrice !== false && (<div>Hotels: {deed.hotels}</div>)}
                    {deed.housePrice !== false && (<div>Houses: {deed.houses}</div>)}
                </div>
                <div className="options">
                    <button disabled={!canTrade} className="trade" onClick={this.onClickTrade.bind(this)}>Add to trade</button>
                </div>
            </div>
        );
    }

    renderPositionDeed(deed) {
        return (
            <div className="property-count">
                {deed.houses > 0 && (<div className="house">{deed.houses}</div>)}
                {deed.hotels > 0 && (<div className="hotel">{deed.hotels}</div>)}
            </div>
        );
    }

    renderPositionPlayers(players) {
        let playerTokens = [];

        for (var p in players) {
            playerTokens.push(
                <img key={p} src={players[p].tokenImage} alt={players[p].username} />
            );
        }

        return playerTokens;
    }

    renderPosition(position, className, nested) {
        let deed = this.getDeedByPosition(position);
        let players = this.getPlayersByPosition(position);

        return (
            <div id={'position-' + position} className={className} onClick={this.onClickPosition.bind(this, position)}>
                {nested}
                {deed && this.renderPositionDeed(deed)}
                {players.length > 0 && this.renderPositionPlayers(players)}
            </div>
        );
    }

    render() {

        let deed = this.getDeedByPosition(this.state.selectedPosition);

        return (
            <div id="game-board-container"><div id="game-board">

                {deed && this.renderSelectedDeed(deed)}

                <div className="row top">
                    {this.renderPosition(21, 'column left')}
                    {this.renderPosition(22, 'column')}
                    {this.renderPosition(23, 'column')}
                    {this.renderPosition(24, 'column')}
                    {this.renderPosition(25, 'column')}
                    {this.renderPosition(26, 'column')}
                    {this.renderPosition(27, 'column')}
                    {this.renderPosition(28, 'column')}
                    {this.renderPosition(29, 'column')}
                    {this.renderPosition(30, 'column')}
                    <div className="column right"></div>
                </div>
                <div className="row side">
                    {this.renderPosition(20, 'column left')}
                    {this.renderPosition(32, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(19, 'column left')}
                    {this.renderPosition(33, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(18, 'column left')}
                    {this.renderPosition(34, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(17, 'column left')}
                    {this.renderPosition(35, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(16, 'column left')}
                    {this.renderPosition(36, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(15, 'column left')}
                    {this.renderPosition(37, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(14, 'column left')}
                    {this.renderPosition(38, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(13, 'column left')}
                    {this.renderPosition(39, 'column right')}
                </div>
                <div className="row side">
                    {this.renderPosition(12, 'column left')}
                    {this.renderPosition(40, 'column right')}
                </div>
                <div className="row bottom">
                    {this.renderPosition(11, 'column left', this.renderPosition(31, 'jail'))}
                    {this.renderPosition(10, 'column')}
                    {this.renderPosition(9, 'column')}
                    {this.renderPosition(8, 'column')}
                    {this.renderPosition(7, 'column')}
                    {this.renderPosition(6, 'column')}
                    {this.renderPosition(5, 'column')}
                    {this.renderPosition(4, 'column')}
                    {this.renderPosition(3, 'column')}
                    {this.renderPosition(2, 'column')}
                    {this.renderPosition(1, 'column right')}
                </div>

            </div></div>
        );
    }
}

export default GameBoard;
