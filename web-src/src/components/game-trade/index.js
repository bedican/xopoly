import React, { Component } from 'react';

import './index.css';

class GameTrade extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.balancePlayerInputTimer = false;
        this.balanceWithPlayerInputTimer = false;

        this.state = {
            trade: null,
            username: false,
            isCurrentPlayer: false,
            player: null,
            players: [],
            playerBalance: '0',
            withPlayerBalance: '0'
        };
    }

    componentWillMount() {
        this.fnTradeState = (data) => { this.onTradeState(data); };
        this.fnTradeAccept = (data) => { this.onTradeAccept(data); };
        this.fnTradeCancel = (data) => { this.onTradeCancel(data); };
        this.fnGameUserList = (data) => { this.onGameUserList(data); };
        this.fnPlayerJoin = (data) => { this.onPlayerJoin(data); };
        this.fnPlayerList = (data) => { this.onPlayerList(data); };
        this.fnPlayerLeave = (data) => { this.onPlayerLeave(data); };
        this.fnPlayState = (data) => { this.onPlayState(data); };

        this.socket.on('trade-state', this.fnTradeState);
        this.socket.on('trade-accept', this.fnTradeAccept);
        this.socket.on('trade-cancel', this.fnTradeCancel);
        this.socket.on('game-user-list', this.fnGameUserList);
        this.socket.on('player-join', this.fnPlayerJoin);
        this.socket.on('player-state', this.fnPlayerJoin);
        this.socket.on('player-list', this.fnPlayerList);
        this.socket.on('player-leave', this.fnPlayerLeave);
        this.socket.on('play-state', this.fnPlayState);
    }

    componentWillUnmount() {
        this.socket.off('trade-state', this.fnTradeState);
        this.socket.off('trade-accept', this.fnTradeAccept);
        this.socket.off('trade-cancel', this.fnTradeCancel);
        this.socket.off('game-user-list', this.fnGameUserList);
        this.socket.off('player-join', this.fnPlayerJoin);
        this.socket.off('player-state', this.fnPlayerJoin);
        this.socket.off('player-list', this.fnPlayerList);
        this.socket.off('player-leave', this.fnPlayerLeave);
        this.socket.off('play-state', this.fnPlayState);
    }

    componentDidMount() {
        if (this.initData.player) {
            this.onPlayerJoin(this.initData.player);
        }

        this.onGameUserList(this.initData.userList);
        this.onPlayerList(this.initData.playerList);
        this.onTradeState(this.initData.trade);
        this.onPlayState(this.initData.playState);
    }

    onTradeState(data) {
        this.setState({
            trade: data,
            playerBalance: data.playerBalance,
            withPlayerBalance: data.withPlayerBalance
        });
    }

    onTradeAccept(data) {
        this.setState({
            trade: null
        });
    }

    onTradeCancel(data) {
        this.setState({
            trade: null
        });
    }

    onGameUserList(data) {
        for(var u in data.users) {
            if (data.users[u].id !== this.initData.userId) {
                continue;
            }

            this.setState({
                username: data.users[u].name
            });
        }
    }

    onPlayerJoin(data) {
        this.setState({
            player: data
        });
    }

    onPlayerList(data) {
        this.setState({
            players: data.players
        });
    }

    onPlayerLeave(data) {
        if (data.userId === this.initData.userId) {
            this.setState({
                player: null,
                isCurrentPlayer: false,
                trade: null
            });
        }
    }

    onPlayState(data) {
        this.setState({
            isCurrentPlayer: ((data.user) && (data.user === this.initData.userId))
        });
    }

    onClickCancelTrade() {
        this.socket.emit('trade-cancel', {});
    }

    onClickOfferTrade() {
        this.socket.emit('trade-offer', {});
    }

    onClickAcceptTrade() {
        this.socket.emit('trade-accept', {});
    }

    onClickCounterTrade() {
        this.socket.emit('trade-counter', {});
    }

    onKeyDownBalanceInput(e) {
        if ((e.keyCode >= 35) && (e.keyCode <= 39)) {
            return true;
        }
        if ((e.which >= 48) && (e.which <= 57)) {
            return true;
        }
        if ((e.which === 46) || (e.which === 8) || (e.which === 9)) {
            return true;
        }

        e.preventDefault();
        return false;
    }

    onChangePlayerBalanceInput(e) {
        var value = e.target.value;

        this.setState({
            playerBalance: value
        });

        if (this.balancePlayerInputTimer) {
            clearTimeout(this.balancePlayerInputTimer);
        }

        this.balancePlayerInputTimer = setTimeout(() => {
            this.socket.emit('trade-balance', {
                player: value
            });
        }, 800);
    }

    onChangeWithPlayerBalanceInput(e) {
        var value = e.target.value;

        this.setState({
            withPlayerBalance: value
        });

        if (this.balanceWithPlayerInputTimer) {
            clearTimeout(this.balanceWithPlayerInputTimer);
        }

        this.balanceWithPlayerInputTimer = setTimeout(() => {
            this.socket.emit('trade-balance', {
                withPlayer: value
            });
        }, 800);
    }

    onFocusBalanceInput(e) {
        if (e.target.value === '0') {
            e.target.select();
        }
    }

    onClickDeed(deedId) {
        if (this.state.trade.isOffered) {
            return;
        }

        this.socket.emit('trade-deed-remove', {
            id: deedId
        });
    }

    renderDeedsList(deeds) {
        if (!deeds.length) {
            return;
        }

        let items = [];

        for (var d in deeds) {
            items.push(
                <li key={d} className="deed">
                    <img src={deeds[d].image} alt={deeds[d].name} onClick={this.onClickDeed.bind(this, deeds[d].id)} />
                </li>
            );
        }

        return (
            <ul>{items}</ul>
        );
    }

    renderPreparingTradingPanelBody() {

        let players = [];

        for(var p in this.state.players) {
            if (this.state.players[p].id !== this.state.player.id) {
                players.push(
                    <option key={p} value={this.state.players[p].id}>
                        {this.state.players[p].username}
                    </option>
                );
            }
        }

        return (
            <div className="panel-body trading trading-prepare">
                <div className="trade">
                    <div className="me">
                        <div className="player">{this.state.username}</div>
                        <div className="balance">
                            <input
                                type="text"
                                disabled={this.state.trade.isOffered}
                                value={this.state.playerBalance}
                                onChange={this.onChangePlayerBalanceInput.bind(this)}
                                onKeyDown={this.onKeyDownBalanceInput.bind(this)}
                                onFocus={this.onFocusBalanceInput.bind(this)}
                            />
                        </div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.playerDeeds)}
                        </div>
                    </div>
                    <div className="them">
                        <div className="player">
                            <div className="select">
                                <select disabled={this.state.trade.isOffered} value={this.state.trade.withPlayer}>
                                    {(players.length > 0) ? players : (<option value=""></option>)}
                                </select>
                            </div>
                        </div>
                        <div className="balance">
                            <input
                                type="text"
                                disabled={this.state.trade.isOffered}
                                value={this.state.withPlayerBalance}
                                onChange={this.onChangeWithPlayerBalanceInput.bind(this)}
                                onKeyDown={this.onKeyDownBalanceInput.bind(this)}
                                onFocus={this.onFocusBalanceInput.bind(this)}
                            />
                        </div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.withPlayerDeeds)}
                        </div>
                    </div>
                </div>
                <div className="options">
                    <button className="offer" onClick={this.onClickOfferTrade.bind(this)} disabled={(this.state.trade.isOffered || !this.state.trade.canOffer)}>Offer</button>
                    <button className="cancel" onClick={this.onClickCancelTrade.bind(this)}>Cancel</button>
                </div>
            </div>
        );
    }

    renderCounterTradingPanelBody() {
        return (
            <div className="panel-body trading trading-counter">
                <div className="trade">
                    <div className="me">
                        <div className="player">{this.state.username}</div>
                        <div className="balance">
                            <input
                                type="text"
                                disabled={this.state.trade.isOffered}
                                value={this.state.playerBalance}
                                onChange={this.onChangePlayerBalanceInput.bind(this)}
                                onKeyDown={this.onKeyDownBalanceInput.bind(this)}
                                onFocus={this.onFocusBalanceInput.bind(this)}
                            />
                        </div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.playerDeeds)}
                        </div>
                    </div>
                    <div className="them">
                        <div className="player">
                            <div className="label">{this.state.trade.withPlayerName}</div>
                        </div>
                        <div className="balance">
                            <input
                                type="text"
                                disabled={this.state.trade.isOffered}
                                value={this.state.withPlayerBalance}
                                onChange={this.onChangeWithPlayerBalanceInput.bind(this)}
                                onKeyDown={this.onKeyDownBalanceInput.bind(this)}
                                onFocus={this.onFocusBalanceInput.bind(this)}
                            />
                        </div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.withPlayerDeeds)}
                        </div>
                    </div>
                </div>
                <div className="options">
                    <button className="offer" onClick={this.onClickOfferTrade.bind(this)} disabled={(this.state.trade.isOffered || !this.state.trade.canOffer)}>Offer</button>
                    <button className="cancel" onClick={this.onClickCancelTrade.bind(this)}>Cancel</button>
                </div>
            </div>
        );
    }

    renderOfferedTradingPanelBody() {

        // A disaled read only view.
        // Which is why player and withPlayer are reversed.

        return (
            <div className="panel-body trading trading-received">
                <div className="trade">
                    <div className="me">
                        <div className="player">{this.state.username}</div>
                        <div className="balance"><input disabled="disabled" type="text" value={this.state.trade.withPlayerBalance} /></div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.withPlayerDeeds)}
                        </div>
                    </div>
                    <div className="them">
                        <div className="player">
                            <div className="label">{this.state.trade.playerName}</div>
                        </div>
                        <div className="balance"><input disabled="disabled" type="text" value={this.state.trade.playerBalance} /></div>
                        <div className="deeds-cards">
                            {this.renderDeedsList(this.state.trade.playerDeeds)}
                        </div>
                    </div>
                </div>
                <div className="options">
                    <button className="accept" onClick={this.onClickAcceptTrade.bind(this)}>Accept</button>
                    {!this.state.isCurrentPlayer && (<button className="counter" onClick={this.onClickCounterTrade.bind(this)}>Counter</button>)}
                    <button className="decline" onClick={this.onClickCancelTrade.bind(this)}>Decline</button>
                </div>
            </div>
        );
    }

    renderPreparingCounterOfferPanelBody() {
        return (
            <div className="panel-body counter">
                A counter offer is currently being prepared.
            </div>
        );
    }

    render() {
        if (!this.state.trade) {
            return null;
        }

        let body = false;
        let isOwner = (this.state.trade.player === this.state.player.id);
        let isRecipient = ((this.state.trade.withPlayer) && (this.state.trade.withPlayer === this.state.player.id));

        let isCounterOfferPreparing = (
            (isRecipient) &&
            (this.state.isCurrentPlayer) &&
            (!this.state.trade.isOffered)
        );

        if (isCounterOfferPreparing) {
            // Waiting for a counter offer
            body = this.renderPreparingCounterOfferPanelBody();
        } else if (isOwner && this.state.isCurrentPlayer) {
            // I am trading with someone
            body = this.renderPreparingTradingPanelBody();
        } else if (isOwner && !this.state.isCurrentPlayer) {
            // I am counter trading with someone
            body = this.renderCounterTradingPanelBody();
        } else if (isRecipient && this.state.trade.isOffered) {
            // Someone is offering a trade with me
            body = this.renderOfferedTradingPanelBody();
        } else {
            return null;
        }

        return (
            <div id="game-info-trade" className="panel">
                <div className="panel-header">Trade</div>
                {body}
            </div>
        );
    }
}

export default GameTrade;
