import React, { Component } from 'react';

import './index.css';

class GameItems extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            username: false,
            player: null,
            isCurrentPlayer: false,
            trade: null,
            balance: false,
            debt: false,
            deeds: [],
            deedId: false,
            cards: [],
            tokens: [],
            token: false
        };
    }

    componentWillMount() {
        this.fnGameUserList = (data) => { this.onGameUserList(data); };
        this.fnPlayerState = (data) => { this.onPlayerState(data); };
        this.fnPlayerLeave = (data) => { this.onPlayerLeave(data); };
        this.fnPlayerBalance = (data) => { this.onPlayerBalance(data); };
        this.fnTokenList = (data) => { this.onTokenList(data); };
        this.fnPlayState = (data) => { this.onPlayState(data); };

        this.socket.on('game-user-list', this.fnGameUserList);
        this.socket.on('player-join', this.fnPlayerState);
        this.socket.on('player-state', this.fnPlayerState);
        this.socket.on('player-leave', this.fnPlayerLeave);
        this.socket.on('player-balance', this.fnPlayerBalance);
        this.socket.on('token-list', this.fnTokenList);
        this.socket.on('play-state', this.fnPlayState);
    }

    componentWillUnmount() {
        this.socket.off('game-user-list', this.fnGameUserList);
        this.socket.off('player-join', this.fnPlayerState);
        this.socket.off('player-state', this.fnPlayerState);
        this.socket.off('player-leave', this.fnPlayerLeave);
        this.socket.off('player-balance', this.fnPlayerBalance);
        this.socket.off('token-list', this.fnTokenList);
        this.socket.off('play-state', this.fnPlayState);
    }

    componentDidMount() {
        if (this.initData.player) {
            this.onPlayerState(this.initData.player);
        }

        this.onTokenList(this.initData.tokenList);
        this.onPlayState(this.initData.playState);
        this.onGameUserList(this.initData.userList);
    }

    getSelectedDeed() {
        for(let s in this.state.deeds) {
            for(var d in this.state.deeds[s]) {
                if (this.state.deeds[s][d].id === this.state.deedId) {
                    return this.state.deeds[s][d];
                }
            }
        }

        return null;
    }

    onGameUserList(data) {
        for(var u in data.users) {
            if (data.users[u].id !== this.initData.userId) {
                continue;
            }

            this.setState({
                username: data.users[u].name
            });
        }
    }

    onPlayerState(player) {
        this.setState({
            player: player,
            balance: player.balance,
            debt: player.debt,
            cards: player.cards,
            deeds: player.deeds
        });
    }

    onPlayerBalance(data) {
        this.setState({
            balance: data.balance,
            debt: data.debt
        });
    }

    onPlayerLeave(data) {
        if (data.userId === this.initData.userId) {
            this.setState({
                player: null,
                isCurrentPlayer: false,
                balance: false,
                debt: false,
                deeds: [],
                cards: [],
                deedId: false
            });
        }
    }

    onTokenList(data) {
        if (!data.tokens) {
            return;
        }
        this.setState({
            token: ((data.tokens) && (data.tokens.length)) ? data.tokens[0].name : false,
            tokens: data.tokens
        });
    }

    onPlayState(data) {
        let isCurrentPlayer = ((data.user) && (data.user === this.initData.userId));
        let enableGetOutOfJailCards = ((isCurrentPlayer) && (!data.roll) && (data.playerInJailRolls > 0));

        this.setState({
            enableGetOutOfJailCards: enableGetOutOfJailCards,
            isCurrentPlayer: isCurrentPlayer,
            trade: data.trade
        });
    }

    onSelectTokenChange(token) {
        this.setState({
            token: token
        });
    }

    onClickJoinGame() {
        this.socket.emit('player-join', {
            token: this.state.token
        });
    }

    onClickLeaveGame() {
        if(window.confirm('Are you sure you wish to leave the game?')) {
            this.socket.emit('player-leave', {});
        }
    }

    onClickPlayCard(id) {
        this.socket.emit('play-card', {
            id: id
        });
    }

    onClickDeed(deedId) {
        this.setState({
            deedId: deedId
        });
    }

    onClickCurrentDeed() {
        this.setState({
            deedId: false
        });
    }

    onClickBuyHouse() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-buy-house', {
            id: deed.id
        });
    }

    onClickSellHouse() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-sell-house', {
            id: deed.id
        });
    }

    onClickBuyHotel() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-buy-hotel', {
            id: deed.id
        });
    }

    onClickSellHotel() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-sell-hotel', {
            id: deed.id
        });
    }

    onClickMortgage() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-mortgage', {
            id: deed.id
        });
    }

    onClickUnmortgage() {
        let deed = this.getSelectedDeed();

        this.socket.emit('deed-unmortgage', {
            id: deed.id
        });
    }

    onClickTrade() {
        this.socket.emit('trade-create', {});
    }

    onClickCurrentDeedTrade() {
        let deed = this.getSelectedDeed();

        this.socket.emit('trade-deed', {
            id: deed.id
        });
    }

    renderTokensSelect() {
        let tokens = [];

        if (this.state.tokens) {
            for(var t in this.state.tokens) {
                tokens.push(
                    <option key={t} value={this.state.tokens[t].name}>
                        {this.state.tokens[t].name}
                    </option>
                );
            }
        }

        return (
            <select disabled={!tokens.length} className="token-list" onChange={(e) => {
                this.onSelectTokenChange(e.target.value);
            }}>
                {tokens}
            </select>
        );
    }

    renderSpectatorPanel() {
        return (
            <div id="game-info-spectator" className="panel">
                <div className="panel-header">You: {this.state.username}</div>
                <div className="panel-body">
                    <div className="join">
                        {this.renderTokensSelect()}
                        <button disabled={!this.state.tokens.length} onClick={this.onClickJoinGame.bind(this)}>
                            Join Game
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    renderSelectedDeed() {
        let deed = this.getSelectedDeed();

        if (!deed) {
            return;
        }

        // can trade if following is met
        // trade does not exist or trade exists and has not yet been offered
        // trade does not exist or trade exists and the deed is not currently in the trade
        // trade does not exist or trade exists and you are the trade player (owner)
        // trade exists or trade does not exist and you are the current player

        let canTrade =
            ((!this.state.trade) || (!this.state.trade.offered)) &&
            ((!this.state.trade) || (!deed.inTrade)) &&
            ((!this.state.trade) || (this.state.trade.player === this.state.player.id)) &&
            ((this.state.trade) || (this.state.isCurrentPlayer));

        let canBuildHouse =
            (this.state.isCurrentPlayer) &&
            (!deed.mortgaged) &&
            (deed.buildable) &&
            (deed.hasSet) &&
            (deed.houses < deed.housesPerHotel) &&
            (((deed.hotels * deed.housesPerHotel) + deed.houses + deed.hotels) < deed.houseLimit) &&
            (deed.housePrice <= this.state.balance);

        let canBuildHotel =
            (this.state.isCurrentPlayer) &&
            (!deed.mortgaged) &&
            (deed.buildable) &&
            (deed.hasSet) &&
            (deed.houses === deed.housesPerHotel) &&
            (deed.housePrice <= this.state.balance);

        let canSellHouse =
            (this.state.isCurrentPlayer) &&
            (deed.houses > 0);

        let canSellHotel =
            (this.state.isCurrentPlayer) &&
            (deed.hotels > 0);

        let canMortgage =
            (this.state.isCurrentPlayer) &&
            (!deed.mortgaged) &&
            (deed.houses === 0) &&
            (deed.hotels === 0);

        let canUnmortgage =
            (this.state.isCurrentPlayer) &&
            (deed.mortgaged) &&
            (this.state.balance >= deed.unmortgagePrice);

        return (
            <div className="deed">
                <div className="image"><img src={deed.image} alt={deed.name} onClick={this.onClickCurrentDeed.bind(this)} /></div>
                <div className="details">
                    <div>Price: {deed.price}</div>
                    <div>Rent: {deed.rental}</div>
                    <div>Mortgaged: {deed.mortgaged ? 'Yes':'No'}</div>
                    <div>Mortgage price: {deed.mortgagePrice}</div>
                    <div>Un-Mortgate price: {deed.unmortgagePrice}</div>
                    {deed.housePrice !== false && (<div>House Price: {deed.housePrice}</div>)}
                    {deed.housePrice !== false && (<div>Hotels: {deed.hotels}</div>)}
                    {deed.housePrice !== false && (<div>Houses: {deed.houses}</div>)}
                </div>
                <div className="options">
                    <div>
                        <button className="buy-house" disabled={!canBuildHouse} onClick={this.onClickBuyHouse.bind(this)}>Buy House</button>
                        <button className="sell-house" disabled={!canSellHouse} onClick={this.onClickSellHouse.bind(this)}>Sell House</button>
                    </div>
                    <div>
                        <button className="buy-hotel" disabled={!canBuildHotel} onClick={this.onClickBuyHotel.bind(this)}>Buy Hotel</button>
                        <button className="sell-hotel" disabled={!canSellHotel} onClick={this.onClickSellHotel.bind(this)}>Sell Hotel</button>
                    </div>
                    <div>
                        <button className="mortgage" disabled={!canMortgage} onClick={this.onClickMortgage.bind(this)}>Mortgage</button>
                        <button className="unmortgage" disabled={!canUnmortgage} onClick={this.onClickUnmortgage.bind(this)}>Unmortgage</button>
                    </div>
                    <div>
                        <button className="trade" disabled={!canTrade} onClick={this.onClickCurrentDeedTrade.bind(this)}>Trade</button>
                    </div>
                </div>
            </div>
        );
    }

    renderDeeds() {
        let deeds = [];

        for(let set in this.state.deeds) {
            let deedSet = [];

            for(var d in this.state.deeds[set]) {
                let deed = this.state.deeds[set][d];

                deedSet.push(
                    <li key={deed.id}>
                        <img src={deed.image} alt={deed.name} onClick={this.onClickDeed.bind(this, deed.id)} />
                    </li>
                );
            }

            deeds.push(<ul key={set}>{deedSet}</ul>);
        }

        return deeds;
    }

    renderCards() {
        let cards = [];

        for(let set in this.state.cards) {
            let cardSet = this.state.cards[set];

            for(var c in cardSet) {

                let canUseCard =
                    (this.state.isCurrentPlayer) &&
                    ((!cardSet[c].outOfJail) || (this.state.enableGetOutOfJailCards));

                cards.push(
                    <li key={cardSet[c].id} className={set}>
                        {cardSet[c].text}
                        <button disabled={!canUseCard} className={(cardSet[c].outOfJail) ? 'get-out-jail' : ''} onClick={this.onClickPlayCard.bind(this, cardSet[c].id)}>Use</button>
                    </li>
                );
            }
        }

        return (
            <ul>{cards}</ul>
        );
    }

    renderItemsPanel() {
        return (
            <div id="game-info-player" className="panel">
                <div className="panel-header">You: {this.state.username}</div>
                <div className="panel-options">
                    <button role="link" className="player-trade" onClick={this.onClickTrade.bind(this)}>Trade</button>
                    <button role="link" className="player-leave" onClick={this.onClickLeaveGame.bind(this)}>Leave Game</button>
                </div>
                <div className="panel-body">
                    <div className="balance">Balance: {this.state.balance} <span className={(this.state.debt > this.state.balance) ? 'in-debt' : ''}>(Debt: {this.state.debt})</span></div>
                    {this.renderSelectedDeed()}
                    <div className="deeds">
                        {this.renderDeeds()}
                    </div>
                    <div className="cards">
                        {this.renderCards()}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (this.state.player) ? this.renderItemsPanel() : this.renderSpectatorPanel();
    }
}

export default GameItems;
