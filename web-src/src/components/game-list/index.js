import React, { Component } from 'react';

import './index.css';

class GameList extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            games: [],
            boards: [],
            board: false
        };
    }

    componentWillMount() {
        this.fnGameList = (data) => { this.onGameList(data); };
        this.socket.on('game-list', this.fnGameList);
    }

    componentWillUnmount() {
        this.socket.off('game-list', this.fnGameList);
    }

    componentDidMount() {
        this.onBoardList(this.initData.boardList);
        this.onGameList(this.initData.gameList);
    }

    onGameList(data) {
        this.setState({
            games: data.games
        });
    }

    onBoardList(data) {
        this.setState({
            board: data.boards.length ? data.boards[0] : false,
            boards: data.boards
        });
    }

    onClickJoinGame(id) {
        this.socket.emit('game-join', {
            id: id
        });
    }

    onClickCreateGame() {
        this.socket.emit('game-create', {
            board: this.state.board
        });
    }

    onSelectBoardChange(board) {
        this.setState({
            board: board
        });
    }

    renderBoardsSelect() {
        let boards = [];

        for(var b in this.state.boards) {
            boards.push(<option key={b} value={this.state.boards[b]}>{this.state.boards[b]}</option>);
        }

        return (
            <select className="board-list" onChange={(e) => {
                this.onSelectBoardChange(e.target.value);
            }}>
                {boards}
            </select>
        );
    }

    renderGamesList() {
        let games = [];

        for(var g in this.state.games) {
            games.push(
                <li key={g}>
                    <button role="link" data-game-id={this.state.games[g].id} onClick={this.onClickJoinGame.bind(this, this.state.games[g].id)}>
                        <div className="game-name">{this.state.games[g].name}</div>
                        <div className="hosted-by">hosted by</div>
                        <div className="username">{this.state.games[g].hostUsername}</div>
                    </button>
                </li>
            );
        }

        return (
            <ul>{games}</ul>
        );
    }

    render() {
        return (
            <div id="game-list">
                <div className="panel">
                    <div className="panel-header">Games</div>
                    <div className="panel-body">
                        <div className="create">
                            {this.renderBoardsSelect()}
                            <button onClick={this.onClickCreateGame.bind(this)}>
                                New Game
                            </button>
                        </div>
                        <div className="games">
                            {this.renderGamesList()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default GameList;
