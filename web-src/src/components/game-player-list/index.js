import React, { Component } from 'react';

import './index.css';

class GamePlayerList extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            player: null,
            players: [],
            deedId: false,
            trade: null,
            isCurrentPlayer: false,
            currentPlayerId: false
        };
    }

    componentWillMount() {
        this.fnPlayerList = (data) => { this.onPlayerList(data); };
        this.fnPlayState = (data) => { this.onPlayState(data); };
        this.fnPlayerState = (data) => { this.onPlayerState(data); };
        this.fnPlayerLeave = (data) => { this.onPlayerLeave(data); };

        this.socket.on('player-list', this.fnPlayerList);
        this.socket.on('play-state', this.fnPlayState);
        this.socket.on('player-join', this.fnPlayerState);
        this.socket.on('player-state', this.fnPlayerState);
        this.socket.on('player-leave', this.fnPlayerLeave);
    }

    componentWillUnmount() {
        this.socket.off('player-list', this.fnPlayerList);
        this.socket.off('play-state', this.fnPlayState);
        this.socket.off('player-join', this.fnPlayerState);
        this.socket.off('player-state', this.fnPlayerState);
        this.socket.off('player-leave', this.fnPlayerLeave);
    }

    componentDidMount() {
        if (this.initData.player) {
            this.onPlayerState(this.initData.player);
        }

        this.onPlayState(this.initData.playState);
        this.onPlayerList(this.initData.playerList);
    }

    getSelectedDeed() {
        let deed;

        for (let p in this.state.players) {
            for(let s in this.state.players[p].deeds) {
                for(var d in this.state.players[p].deeds[s]) {
                    if (this.state.players[p].deeds[s][d].id === this.state.deedId) {

                        deed = this.state.players[p].deeds[s][d];
                        deed.player = this.state.players[p];

                        return deed;
                    }
                }
            }
        }

        return null;
    }

    onPlayerState(player) {
        this.setState({
            player: player
        });
    }

    onPlayerList(data) {
        this.setState({
            players: data.players
        });
    }

    onPlayerLeave(data) {
        if (data.userId === this.initData.userId) {
            this.setState({
                player: null,
                isCurrentPlayer: false,
                currentPlayerId: false,
                trade: null,
                deedId: false
            });
        }
    }

    onPlayState(data) {
        let isCurrentPlayer = ((data.user) && (data.user === this.initData.userId));

        this.setState({
            isCurrentPlayer: isCurrentPlayer,
            trade: data.trade,
            currentPlayerId: data.player
        });
    }

    onClickDeed(deedId) {
        this.setState({
            deedId: deedId
        });
    }

    onClickCurrentDeed() {
        this.setState({
            deedId: false
        });
    }

    onClickCurrentDeedTrade() {
        let deed = this.getSelectedDeed();

        this.socket.emit('trade-deed', {
            id: deed.id
        });
    }

    renderSelectedDeed() {
        let deed = this.getSelectedDeed();

        if (!deed) {
            return;
        }

        let canTrade = (
            (this.state.trade) &&
            (!deed.inTrade) &&
            (!this.state.trade.offered) &&
            (this.state.player.id === this.state.trade.player) &&
            ((!this.state.trade.withPlayer) || (deed.player.id === this.state.trade.withPlayer))
        ) || (
            (!this.state.trade) && (this.state.isCurrentPlayer)
        );

        return (
            <div className="deed">
                <div className="image"><img src={deed.image} alt={deed.name} onClick={this.onClickCurrentDeed.bind(this)} /></div>
                <div className="details">
                    <div>Price: {deed.price}</div>
                    <div>Rent: {deed.rental}</div>
                    <div>Mortgaged: {deed.mortgaged ? 'Yes':'No'}</div>
                    {deed.housePrice !== false && (<div>House Price: {deed.housePrice}</div>)}
                    {deed.housePrice !== false && (<div>Hotels: {deed.hotels}</div>)}
                    {deed.housePrice !== false && (<div>Houses: {deed.houses}</div>)}
                </div>
                <div className="options">
                    <button className="trade" disabled={!canTrade} onClick={this.onClickCurrentDeedTrade.bind(this)}>Trade</button>
                </div>
            </div>
        );
    }

    renderDeeds(player) {
        let deeds = [];

        for(let set in player.deeds) {
            let deedSet = [];

            for(var d in player.deeds[set]) {
                let deed = player.deeds[set][d];

                deedSet.push(
                    <li key={deed.id}>
                        <img src={deed.image} alt={deed.name} onClick={this.onClickDeed.bind(this, deed.id)} />
                    </li>
                );
            }

            deeds.push(<ul key={set}>{deedSet}</ul>);
        }

        if (!deeds.length) {
            return;
        }

        return (<div className="deeds">{deeds}</div>);
    }

    renderPlayersList() {
        let players = [];

        for(var p in this.state.players) {
            if (this.state.players[p].userId === this.initData.userId) {
                continue;
            }

            players.push(
                <li key={p} className={this.state.currentPlayerId === this.state.players[p].id ? 'current' : ''}>
                    <div className="player-name">
                        {this.state.players[p].token} - {this.state.players[p].username} ({this.state.players[p].balance} / {this.state.players[p].debt})
                    </div>
                    {this.renderDeeds(this.state.players[p])}
                </li>
            );
        }

        if (!players.length) {
            return (<div>No other users are currently playing.</div>);
        }

        return (
            <ul className="players">{players}</ul>
        );
    }

    render() {
        return (
            <div id="game-info-players" className="panel">
                <div className="panel-header">Players</div>
                <div className="panel-body">
                    {this.renderSelectedDeed()}
                    {this.renderPlayersList()}
                </div>
            </div>
        );
    }
}

export default GamePlayerList;
