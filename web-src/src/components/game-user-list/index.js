import React, { Component } from 'react';

import './index.css';

class GameUserList extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            host: false,
            users: []
        };
    }

    componentWillMount() {
        this.fnGameUserList = (data) => { this.onGameUserList(data); };
        this.socket.on('game-user-list', this.fnGameUserList);
    }

    componentWillUnmount() {
        this.socket.off('game-user-list', this.fnGameUserList);
    }

    componentDidMount() {
        this.onGameUserList(this.initData.userList);
    }

    onGameUserList(data) {
        this.setState({
            host: data.host,
            users: data.users
        });
    }

    renderUserList() {
        let users = [];

        for(var u in this.state.users) {
            if (this.state.users[u].id === this.state.host) {
                users.push(<li key={u}>{this.state.users[u].name} (Host)</li>);
            } else {
                users.push(<li key={u}>{this.state.users[u].name}</li>);
            }
        }

        return (
            <ul>{users}</ul>
        );
    }

    render() {
        return (
            <div id="game-info-users" className="panel">
                <div className="panel-header">Users</div>
                <div className="panel-body">
                    {this.renderUserList()}
                </div>
            </div>
        );
    }
}

export default GameUserList;
