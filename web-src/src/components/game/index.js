import React, { Component } from 'react';

import GameBoard from '../game-board'
import GameChat from '../game-chat'
import GamePlay from '../game-play'
import GameTrade from '../game-trade'
import GameItems from '../game-items'
import GamePlayerList from '../game-player-list'
import GameUserList from '../game-user-list'

import './index.css';

class Game extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
        };
    }

    render() {
        return (
            <div id="game">

                <GameBoard socket={this.socket} initData={this.initData} />

                <div id="game-info">
                    <GameChat socket={this.socket} initData={this.initData} />
                    <GamePlay socket={this.socket} initData={this.initData} />
                    <GameTrade socket={this.socket} initData={this.initData} />
                    <GameItems socket={this.socket} initData={this.initData} />
                    <GamePlayerList socket={this.socket} initData={this.initData} />
                    <GameUserList socket={this.socket} initData={this.initData} />
                </div>

            </div>
        );
    }
}

export default Game;
