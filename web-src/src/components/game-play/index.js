import React, { Component } from 'react';

import './index.css';

class GamePlay extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            gameName: this.initData.game.name,
            player: null,
            balance: false,
            debt: false,
            players: [],
            isCurrentPlayer: false,
            currentRoll: null,
            currentPlayerName: false,
            currentPlayerInJailRolls: false,
            currentDeed: null,
            lastCard: null,
            currentTrade: null,
            outOfjailFee: false
        };
    }

    componentWillMount() {
        this.fnPlayState = (data) => { this.onPlayState(data); };
        this.fnPlayerJoin = (data) => { this.onPlayerJoin(data); };
        this.fnPlayerLeave = (data) => { this.onPlayerLeave(data); };
        this.fnPlayerBalance = (data) => { this.onPlayerBalance(data); };
        this.fnPlayerList = (data) => { this.onPlayerList(data); };
        this.fnGameList = (data) => { this.onGameList(data); };

        this.socket.on('play-state', this.fnPlayState);
        this.socket.on('player-join', this.fnPlayerJoin);
        this.socket.on('player-state', this.fnPlayerJoin);
        this.socket.on('player-leave', this.fnPlayerLeave);
        this.socket.on('player-balance', this.fnPlayerBalance);
        this.socket.on('player-list', this.fnPlayerList);
        this.socket.on('game-list', this.fnGameList);
    }

    componentWillUnmount() {
        this.socket.off('play-state', this.fnPlayState);
        this.socket.off('player-join', this.fnPlayerJoin);
        this.socket.off('player-state', this.fnPlayerJoin);
        this.socket.off('player-leave', this.fnPlayerLeave);
        this.socket.off('player-balance', this.fnPlayerBalance);
        this.socket.off('player-list', this.fnPlayerList);
        this.socket.off('game-list', this.fnGameList);
    }

    componentDidMount() {
        if (this.initData.player) {
            this.onPlayerJoin(this.initData.player);
        }
        if (this.initData.playerList) {
            this.onPlayerList(this.initData.playerList);
        }

        this.onPlayState(this.initData.playState);
    }

    onGameList(data) {
        for(var g in data.games) {
            if (data.games[g].id === this.initData.game.id) {
                this.setState({
                    gameName: data.games[g].name
                });

                return;
            }
        }
    }

    onPlayerJoin(player) {
        this.setState({
            player: player,
            balance: player.balance,
            debt: player.debt
        });
    }

    onPlayerLeave(data) {
        if (data.userId === this.initData.userId) {
            this.setState({
                player: null,
                balance: false,
                debt: false,
                isCurrentPlayer: false
            });
        }
    }

    onPlayerBalance(data) {
        this.setState({
            balance: data.balance,
            debt: data.debt
        });
    }

    onPlayerList(data) {
        this.setState({
            players: data.players
        });
    }

    onPlayState(data) {
        this.setState({
            isCurrentPlayer: ((data.user) && (data.user === this.initData.userId)),
            currentRoll: data.roll,
            currentPlayerId: data.player,
            currentPlayerName: data.playerName,
            currentPlayerInJailRolls: data.playerInJailRolls,
            currentDeed: data.deed,
            lastCard: data.card,
            outOfjailFee: data.outOfjailFee,
            currentTrade: data.trade
        });
    }

    onClickLeave() {
        if(window.confirm('Are you sure you wish to leave the game?')) {
            this.socket.emit('game-leave', {});
        }
    }

    onClickRoll() {
        this.socket.emit('play-roll', {});
    }

    onClickJailPay() {
        this.socket.emit('play-jail-pay', {});
    }

    onClickComplete() {
        this.socket.emit('play-complete', {});
    }

    onClickBuyDeed() {
        this.socket.emit('deed-buy', {});
    }

    renderWaitingPanelBody() {
        return (
            <div className="panel-body waiting">
                Waiting for players...
            </div>
        );
    }

    renderWatchingPanelBody() {
        let resultRoll = 'Waiting for ' + this.state.currentPlayerName + ' to roll';
        let resultMessage = '';

        if (this.state.currentRoll) {
            resultRoll = 'Rolled a ' + this.state.currentRoll.first + ' and ' + this.state.currentRoll.second;

            if (this.state.currentPlayerInJailRolls > 0) {
                resultMessage = 'In Jail, ' + this.state.currentPlayerInJailRolls + ' more dice rolls to get out.';
            }

            if (this.state.currentDeed) {
                resultMessage = this.state.currentPlayerName + ' has landed on "' + this.state.currentDeed.name + '"';
                if (this.state.currentDeed.player) {
                    if ((this.state.player) && (this.state.currentDeed.player === this.state.player.id)) {
                        resultMessage += ' owned by you';
                        if (!this.state.currentDeed.traded) {
                            resultMessage += ' and has paid you ' + this.state.currentDeed.rental + ' in rent';
                        }
                    } else {
                        resultMessage += ' owned by "' + this.state.currentDeed.playerName + '"';
                        if ((this.state.currentDeed.player !== this.state.currentPlayerId) && (!this.state.currentDeed.traded)) {
                            resultMessage += ' and has paid ' + this.state.currentDeed.rental + ' in rent';
                        }
                    }
                } else {
                    resultMessage += ' valued at ' + this.state.currentDeed.price;
                }
            }

            if (this.state.lastCard) {
                resultMessage = this.state.currentPlayerName + ' has picked up the card: "' + this.state.lastCard.text + '"';
            }
        }

        return (
            <div className="panel-body watching">
                <div className="result">
                    <div className="dice">{resultRoll}</div>
                    <div className="message">{resultMessage}</div>
                </div>
                {(this.state.currentRoll && this.state.currentDeed) &&
                    <div className="deed">
                        <div className="image"><img src={this.state.currentDeed.image} alt={this.state.currentDeed.name} /></div>
                    </div>
                }
            </div>
        );
    }

    renderRollPanelBody() {

        let message = '';
        let payButtonDisabled = true;

        if (this.state.currentPlayerInJailRolls > 0) {
            message = 'You are currently in jail, roll a double';
            if (this.state.balance >= this.state.outOfjailFee) {
                message += ', pay ' + this.state.outOfjailFee;
                payButtonDisabled = false;
            }

            message += ', or roll ' + this.state.currentPlayerInJailRolls + ' more times to get out of jail.';
        }

        return (
            <div className="panel-body roll">
                <div className="message">{message}</div>
                <div>
                    <button disabled={this.state.currentTrade} className="roll" onClick={this.onClickRoll.bind(this)}>Roll</button>
                </div>
                {(this.state.currentPlayerInJailRolls > 0) &&
                    <div className="get-out-jail">
                        <button disabled={payButtonDisabled} className="pay" onClick={this.onClickJailPay.bind(this)}>Pay</button>
                    </div>
                }
            </div>
        );
    }

    renderPlayingPanelBody() {

        let resultRoll = '';
        let resultMessage = '';
        let deedBuyButtonDisabled = true;
        let completeButtonDisabled = true;
        let isInDebt = (this.state.debt > this.state.balance);

        if (this.state.currentRoll) {
            resultRoll = 'Rolled a ' + this.state.currentRoll.first + ' and ' + this.state.currentRoll.second;
        }

        if (this.state.currentPlayerInJailRolls > 0) {
            resultMessage = 'In Jail, ' + this.state.currentPlayerInJailRolls + ' more dice rolls to get out.';
        }

        if (this.state.currentDeed) {
            resultMessage = 'You have landed on "' + this.state.currentDeed.name + '"';

            if (this.state.currentDeed.player) {
                if (this.state.currentDeed.player === this.state.player.id) {
                    resultMessage += ' owned by you';
                } else {
                    resultMessage += ' owned by "' + this.state.currentDeed.playerName + '"';
                    if (!this.state.currentDeed.traded) {
                        resultMessage += ' and have paid ' + this.state.currentDeed.rental + ' for rent';
                    }
                }
            } else if (this.state.balance >= this.state.currentDeed.price) {
                resultMessage += ' you can buy this for ' + this.state.currentDeed.price;
            } else {
                resultMessage += ' valued at ' + this.state.currentDeed.price;
            }

            // Not owned and can afford it
            if ((!this.state.currentDeed.player) && (this.state.balance >= this.state.currentDeed.price)) {
                deedBuyButtonDisabled = false;
            }
        }

        if (this.state.lastCard) {
            resultMessage = 'You have picked up the card: "' + this.state.lastCard.text + '"';
        }

        if (!this.state.currentTrade) {
            completeButtonDisabled = false;
        }

        return (
            <div className="panel-body playing">
                <div className="result">
                    <div className="dice">{resultRoll}</div>
                    <div className="message">{resultMessage}</div>
                    <button disabled={completeButtonDisabled} className={'complete' + (isInDebt ? ' in-debt' : '')} onClick={this.onClickComplete.bind(this)}>Complete</button>
                </div>
                {this.state.currentDeed &&
                    <div className="deed">
                        <div className="image"><img src={this.state.currentDeed.image} alt={this.state.currentDeed.name} /></div>
                        <button disabled={deedBuyButtonDisabled} className="buy" onClick={this.onClickBuyDeed.bind(this)}>Buy</button>
                    </div>
                }
            </div>
        );
    }

    render() {
        return (
            <div id="game-info-current" className="panel">
                <div className="panel-header">{this.state.gameName}</div>
                <div className="panel-options">
                    <button role="link" className="game-leave" onClick={this.onClickLeave.bind(this)}>Leave</button>
                </div>

                {(!this.state.players.length) && this.renderWaitingPanelBody()}
                {(!this.state.isCurrentPlayer && this.state.players.length > 0) && this.renderWatchingPanelBody()}
                {(this.state.isCurrentPlayer && !this.state.currentRoll) && this.renderRollPanelBody()}
                {(this.state.isCurrentPlayer && this.state.currentRoll) && this.renderPlayingPanelBody()}

            </div>
        );
    }
}

export default GamePlay;
