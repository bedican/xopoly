import React, { Component } from 'react';

import './index.css';

class GameChat extends Component {

    constructor(props) {
        super(props);

        this.socket = props.socket;
        this.initData = props.initData;

        this.state = {
            message: '',
            messages: [
                { messages: ['Type /help for a list of commands'] }
            ]
        };
    }

    componentWillMount() {
        this.fnChatMessage = (data) => { this.onChatMessage(data); };
        this.socket.on('chat-message', this.fnChatMessage);
    }

    componentWillUnmount() {
        this.socket.off('chat-message', this.fnChatMessage);
    }

    componentDidUpdate() {
        let container = document.getElementById('game-info-chat-messages');
        container.scrollTop = container.scrollHeight;
    }

    onChatMessage(data) {

        let messages = this.state.messages;
        messages.push(data);

        this.setState({
            messages: messages
        });
    }

    onSendPress() {
        this.socket.emit('chat-message', {
            message: this.state.message.substring(0, 400)
        });

        this.setState({
            message: ''
        });
    }

    onMessageChange(e) {
        this.setState({
            message: e.target.value
        });
    }

    onMessageKeyPress(e) {
        if (e.key === 'Enter') {
            this.onSendPress();
        }
    }

    render() {

        let messages = [];

        for(var m in this.state.messages) {
            let username = this.state.messages[m].username;

            for(var k in this.state.messages[m].messages) {
                messages.push(
                    <li key={(m + 1) * (k + 1)} className={username ? 'chat' : 'system'}>
                        <span className="username">{username ? (username + ': ') : '* '}</span>
                        <span>{this.state.messages[m].messages[k]}</span>
                    </li>
                );
            }
        }

        return (
            <div id="game-info-chat" className="panel">
                <div className="panel-header">Chat</div>
                <div className="panel-body">
                    <div className="messages" id="game-info-chat-messages">
                        <ul>{messages}</ul>
                    </div>
                    <div className="message">
                        <input type="text" name="message" value={this.state.message} onChange={this.onMessageChange.bind(this)} onKeyPress={this.onMessageKeyPress.bind(this)} />
                        <button onClick={() => {
                            this.onSendPress();
                        }}>Send</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default GameChat;
