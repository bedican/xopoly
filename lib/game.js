var GameState = require('./game-state');
var idgen = require('./id-gen');

var Game = function(name, user, board) {
    this.name = name;
    this.user = user;
    this.board = board;

    this.users = [];
    this.bannedIp = [];

    this.id = idgen.uniqueId();
    this.state = new GameState(this.board);
    this.users.push(this.user);
};

Game.prototype.export = function() {

    var users = [];
    for(var key in this.users) {
        users.push(this.users[key].export());
    }

    return {
        id: this.id,
        name: this.name,
        user: this.user.getId(),
        users: users,
        board: this.board.export(),
        state: this.state.export()
    };
};

Game.prototype.getId = function() {
    return this.id;
};

Game.prototype.getName = function() {
    return this.name;
};

Game.prototype.setName = function(name) {
    this.name = name;
};

Game.prototype.getBoard = function() {
    return this.board;
};

Game.prototype.getState = function() {
    return this.state;
};

Game.prototype.getHostUser = function() {
    return this.user;
};

Game.prototype.setHostUser = function(user) {
    if (!this.hasUser(user)) {
        return false;
    }

    this.user = user;
    return true;
};

Game.prototype.addUser = function(user) {
    if (this.hasUser(user)) {
        return;
    }

    this.users.push(user);
};

Game.prototype.getUsers = function() {
    return this.users;
};

Game.prototype.hasUsers = function() {
    return (this.users.length > 0);
};

Game.prototype.hasUser = function(user) {
    return (this.users.indexOf(user) != -1);
};

Game.prototype.removeUser = function(user) {
    var index = this.users.indexOf(user);
    if (index == -1) {
        return;
    }

    this.state.removePlayer(user);
    this.users.splice(index, 1);
};

Game.prototype.reassignHostUser = function() {
    if (!this.users.length) {
        return;
    }

    this.user = this.users[0];
};

Game.prototype.getUserByUsername = function(username) {
    for(var key in this.users) {
        if (this.users[key].getName().trim().toLowerCase() == username.trim().toLowerCase()) {
            return this.users[key];
        }
    }

    return false;
};

Game.prototype.addBannedUser = function(user) {
    if (!this.hasUser(user)) {
        return;
    }
    if (this.isBannedUser(user)) {
        return;
    }
    if (this.user.getClientIp() == user.getClientIp()) {
        // Can't ban the hosts ip
        return;
    }

    // remove all users of the same ip address.
    // since we are using the ip to ban.

    var bannedUsers = [];

    for (var key in this.users) {
        if (this.users[key].getClientIp() == user.getClientIp()) {
            this.removeUser(this.users[key]);
            bannedUsers.push(this.users[key]);
        }
    }

    this.bannedIp.push(user.getClientIp());

    return bannedUsers;
};

Game.prototype.removeBannedIp = function(ip) {
    var index = this.bannedIp.indexOf(ip);
    if (index == -1) {
        return false;
    }

    this.bannedIp.splice(index, 1);
    return true;
};

Game.prototype.isBannedUser = function(user) {
    return (this.bannedIp.indexOf(user.getClientIp()) != -1);
};

Game.prototype.getBannedIp = function() {
    return this.bannedIp;
};

module.exports = Game;
