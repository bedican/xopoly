var Player = require('../player');
var formatter = require('../formatters/formatter');

var methods = {
    cancelTradeForPlayers: function(state, player) {
        var trade = state.getTrade();

        if (trade) {
            var ownerPlayer = trade.getPlayer();
            var withPlayer = trade.getWithPlayer();

            if ((player.getId() == ownerPlayer.getId()) || (player.getId() == withPlayer.getId())) {
                ownerPlayer.getUser().getSocket().emit('trade-cancel', {});
                if (withPlayer) {
                    withPlayer.getUser().getSocket().emit('trade-cancel', {});
                }
            }
        }
    }
};

var events = {

    on: {
        join: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var token = data.token;

            if (!token) {
                return;
            }

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = new Player(user, board.getTokenBag().getToken(token), board.getConfig().getStartBalance());

            if (!state.addPlayer(player)) {
                return;
            }

            state.getPlayerTimer(player).onTimeout(function(player) {

                methods.cancelTradeForPlayers(state, player);

                io.to(game.getId()).emit('player-leave', formatter.player.leave(player));
                io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
                io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
                io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' is no longer playing in the game (timed out)'));
                io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            });

            socket.emit('player-join', formatter.player.player(game, state.getPlayer(user)));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' is now playing in the game (' + data.token + ')'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        },
        leave: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = state.getPlayer(user);

            methods.cancelTradeForPlayers(state, player);

            state.removePlayer(user);

            io.to(game.getId()).emit('player-leave', formatter.player.leave(player));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' is no longer playing in the game'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        }
    },
    bind: function(server, user) {

        var _this = this;

        var io = server.getIo();
        var socket = user.getSocket();

        socket.on('player-join', function(data) {
            _this.on.join(server, user, data);
        });

        socket.on('player-leave', function(data) {
            _this.on.leave(server, user, data);
        });

        this.init(server, user, socket);
    },
    init: function(server, user, socket) {

    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
