var formatter = require('../formatters/formatter');

var events = {

    run: function(server, game, user, command) {
        var args = command.split(' ');
        if (this.command[args[0]]) {
            this.command[args[0]](server, game, user, args.slice(1));
        }
    },

    command: {
        nick: function(server, game, user, args) {
            var username = args.join(' ');
            if (!username) {
                return;
            }

            if(!username.match(/^[\s0-9a-z_-]+$/gi)) {
                user.getSocket().emit('chat-message', formatter.chat.system('Invalid username'));
                return;
            }

            username = username.replace(/\s+/g, ' ').trim();

            if (server.isUsername(username)) {
                user.getSocket().emit('chat-message', formatter.chat.system('Username already in use'));
                return;
            }

            var io = server.getIo();
            var state = game.getState();
            var oldUsername = user.getName();

            user.setName(username);

            io.emit('game-list', formatter.game.games(server.getGames()));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(oldUsername + ' is now known as ' + username));
        },
        gamenick: function(server, game, user, args) {
            var name = args.join(' ');
            if (!name) {
                return;
            }

            if(!name.match(/^[\s0-9a-z_-]+$/gi)) {
                user.getSocket().emit('chat-message', formatter.chat.system('Invalid name'));
                return;
            }

            name = name.replace(/\s+/g, ' ').trim();

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            if (server.isGameName(name)) {
                user.getSocket().emit('chat-message', formatter.chat.system('Game name already in use'));
                return;
            }

            var io = server.getIo();

            game.setName(name);

            io.emit('game-list', formatter.game.games(server.getGames()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system('Game is now known as ' + name));
        },
        json: function(server, game, user, args) {
            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            user.getSocket().emit('chat-message', formatter.chat.system(JSON.stringify(game.export())));
        },
        kick: function(server, game, user, args) {
            var username = args.join(' ');
            if (!username) {
                return;
            }

            username = username.replace(/\s+/g, ' ').trim();

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var kickUser = game.getUserByUsername(username);

            if ((!kickUser) || (kickUser.getId() == user.getId())) {
                return;
            }

            var io = server.getIo();

            var state = game.getState();
            var board = game.getBoard();

            var kickPlayer = state.getPlayer(kickUser);

            state.removePlayer(kickUser);

            kickUser.getSocket().emit('chat-message', formatter.chat.system('You have been removed from the game by the host'));

            io.to(game.getId()).emit('player-leave', formatter.player.leave(kickPlayer));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(kickUser.getName() + ' is no longer playing in the game (kicked)'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        },
        ban: function(server, game, user, args) {
            var username = args.join(' ');
            if (!username) {
                return;
            }

            username = username.replace(/\s+/g, ' ').trim();

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var banUser = game.getUserByUsername(username);

            if ((!banUser) || (banUser.getId() == user.getId())) {
                return;
            }

            var io = server.getIo();
            var socket = banUser.getSocket();

            var state = game.getState();
            var board = game.getBoard();

            var bannedUsers = game.addBannedUser(banUser);

            for(var key in bannedUsers) {
                bannedUsers[key].getSocket().leave(game.getId());
                bannedUsers[key].getSocket().emit('game-leave', {});
                io.to(game.getId()).emit('chat-message', formatter.chat.system(bannedUsers[key].getName() + ' has left the game (banned)'));
            }

            io.emit('game-list', formatter.game.games(server.getGames()));
            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        },
        unban: function(server, game, user, args) {
            var ip = args.shift();

            if (!ip) {
                return;
            }

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var success = game.removeBannedIp(ip);

            if (success) {
                user.getSocket().emit('chat-message', formatter.chat.system('Ip address "' + ip + '" has been unbanned'));
            }
        },
        banlist: function(server, game, user, args) {
            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var ips = game.getBannedIp();

            if (!ips.length) {
                user.getSocket().emit('chat-message', formatter.chat.system('No ip addresses banned. Yay!'));
                return;
            }

            var messages = [
                'Banned ip addresses:'
            ];

            for(var key in ips) {
                messages.push(ips[key]);
            }

            user.getSocket().emit('chat-message', formatter.chat.multi(messages));
        },
        ip: function (server, game, user, args) {
            var username = args.join(' ');
            if (!username) {
                return;
            }

            username = username.replace(/\s+/g, ' ').trim();

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var ipUser = game.getUserByUsername(username);

            if (!ipUser) {
                return;
            }

            user.getSocket().emit('chat-message', formatter.chat.system('User ' + username + ' has an ip address of ' + ipUser.getClientIp()));
        },
        host: function(server, game, user, args) {
            var username = args.join(' ');
            if (!username) {
                return;
            }

            username = username.replace(/\s+/g, ' ').trim();

            if (game.getHostUser().getId() != user.getId()) {
                return;
            }

            var hostUser = game.getUserByUsername(username);

            if ((!hostUser) || (hostUser.getId() == user.getId())) {
                return;
            }

            var io = server.getIo();

            if(!game.setHostUser(hostUser)) {
                return;
            }

            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(hostUser.getName() + ' is now the host of this game'));
        },
        help: function(server, game, user, args) {

            var messages = [
                'Commands:',
                '/nick <username> - Change your username'
            ];

            if (game.getHostUser().getId() == user.getId()) {
                messages.push('/gamenick <name> - Change the name of the game');
                messages.push('/kick <username> - Remove <username> as a player');
                messages.push('/host <username> - Set the host user for this game');
                messages.push('/ban <username> - Bans all users using <username> ip address from the game');
                messages.push('/unban <ip> - Unbans the <ip> address from the game');
                messages.push('/banlist - List currently banned ip addresses');
                messages.push('/ip <username> - Display the ip address of <username>');
            }

            user.getSocket().emit('chat-message', formatter.chat.multi(messages));
        }
    },

    on: {
        message: function(server, user, data) {
            if (!data.message) {
                return;
            }

            data.message = data.message.substring(0, 400);

            var io = server.getIo();
            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            if (data.message.substring(0, 1) == '/') {
                events.run(server, game, user, data.message.substring(1));
                user.getSocket().emit('chat-message', formatter.chat.message(user, false));
            } else {
                io.to(game.getId()).emit('chat-message', formatter.chat.message(user, data.message));
            }
        }
    },

    bind: function(server, user) {

        var _this = this;

        var socket = user.getSocket();

        socket.on('chat-message', function(data) {
            _this.on.message(server, user, data);
        });

        this.init(server, user, socket);
    },

    init: function(server, user, socket) {
    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
