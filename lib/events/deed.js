var formatter = require('../formatters/formatter');

var events = {

    on: {
        buy: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = state.getLastDeed();

            if (!deed) {
                return;
            }

            var deedPlayer = board.getDeedDeck().getDeedOwner(state, deed);

            if (deedPlayer) {
                return;
            }

            var price = deed.getPrice();

            if (!player.getWallet().hasBalance(price)) {
                return;
            }

            state.tickActiveTimer();

            player.getWallet().removeBalance(price);
            player.getWallet().addDeed(deed);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has bought ' + deed.getName() + ' for ' + deed.getPrice()));
        },
        mortgage: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = game.getBoard().getDeedDeck().getDeed(data.id);

            if ((!deed) || (deed.hasProperty())) {
                return;
            }

            state.tickActiveTimer();

            var price = deed.mortgage();
            player.getWallet().addBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has mortgaged ' + deed.getName() + ' for ' + price));
        },
        unmortgage: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = game.getBoard().getDeedDeck().getDeed(data.id);

            if ((!deed) || (!deed.isMortgaged())) {
                return;
            }

            var price = deed.getUnmortgagePrice();

            if (!player.getWallet().hasBalance(price)) {
                return;
            }

            state.tickActiveTimer();

            deed.unmortgage();
            player.getWallet().removeBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has unmortgaged ' + deed.getName() + ' for ' + price));
        },
        buyHouse: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = board.getDeedDeck().getDeed(data.id);

            if ((!deed) || (!deed.isBuildable()) || (deed.isMortgaged())) {
                return;
            }

            if (!board.getDeedDeck().hasDeedSet(player, deed.getSet())) {
                return;
            }

            var price = deed.getHousePrice();

            if (!player.getWallet().hasBalance(price)) {
                return;
            }

            var houses = deed.addHouse();

            if (!houses) {
                return;
            }

            state.tickActiveTimer();

            player.getWallet().removeBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has bought a house for ' + deed.getName()));
        },
        buyHotel: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = board.getDeedDeck().getDeed(data.id);

            if ((!deed) || (!deed.isBuildable()) || (deed.isMortgaged())) {
                return;
            }

            if (!board.getDeedDeck().hasDeedSet(player, deed.getSet())) {
                return;
            }

            var price = deed.getHousePrice();

            if (!player.getWallet().hasBalance(price)) {
                return;
            }

            var hotels = deed.addHotel();

            if (!hotels) {
                return;
            }

            state.tickActiveTimer();
            player.getWallet().removeBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has bought a hotel for ' + deed.getName()));
        },
        sellHouse: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = game.getBoard().getDeedDeck().getDeed(data.id);

            if ((!deed) || (!deed.isBuildable()) || (!deed.getHouses())) {
                return;
            }

            state.tickActiveTimer();

            var price = deed.getHouseSalePrice();

            deed.removeHouse();
            player.getWallet().addBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has sold a house on ' + deed.getName()));
        },
        sellHotel: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var deed = game.getBoard().getDeedDeck().getDeed(data.id);

            if ((!deed) || (!deed.isBuildable()) || (!deed.getHotels())) {
                return;
            }

            state.tickActiveTimer();

            var price = deed.getHouseSalePrice();

            deed.removeHotel();
            player.getWallet().addBalance(price);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has sold a house on ' + deed.getName()));
        }
    },

    bind: function(server, user) {

        var _this = this;

        var socket = user.getSocket();

        socket.on('deed-buy', function(data) {
            _this.on.buy(server, user, data);
        });

        socket.on('deed-mortgage', function(data) {
            _this.on.mortgage(server, user, data);
        });

        socket.on('deed-unmortgage', function(data) {
            _this.on.unmortgage(server, user, data);
        });

        socket.on('deed-buy-house', function(data) {
            _this.on.buyHouse(server, user, data);
        });

        socket.on('deed-buy-hotel', function(data) {
            _this.on.buyHotel(server, user, data);
        });

        socket.on('deed-sell-house', function(data) {
            _this.on.sellHouse(server, user, data);
        });

        socket.on('deed-sell-hotel', function(data) {
            _this.on.sellHotel(server, user, data);
        });

        this.init(server, user, socket);
    },

    init: function(server, user, socket) {

    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
