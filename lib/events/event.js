var modules = [
    'game',
    'player',
    'chat',
    'play',
    'deed',
    'trade'
];

module.exports = {
    bind: function(server, user) {
        for(var key in modules) {
            require('./' + modules[key]).bind(server, user);
        }
    }
};
