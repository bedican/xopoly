var formatter = require('../formatters/formatter');

var events = {

    on: {
        roll: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            if (state.getTrade()) {
                return;
            }

            var roll = state.rollDice();

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has rolled a ' + roll.first + ' and a ' + roll.second));

            // The action of the roll may have updated everyones balance.
            var players = state.getPlayers();
            for(var key in players) {
                players[key].getUser().getSocket().emit('player-balance', formatter.player.balance(players[key]));
            }
        },
        complete: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            if (state.getTrade()) {
                return;
            }

            state.nextPlayer();

            if (player.getDebt().isOutstanding()) {
                io.to(game.getId()).emit('player-leave', formatter.player.leave(player));
                io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
                io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' is now bankrupt and is no longer playing in the game'));
            } else {
                socket.emit('player-state', formatter.player.player(game, player));
            }

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
        },
        jailPay: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var outOfJailFee = game.getBoard().getConfig().getOutOfJailFee();

            if (!player.getWallet().hasBalance(outOfJailFee)) {
                return;
            }

            state.tickActiveTimer();

            player.getWallet().removeBalance(outOfJailFee);
            state.movePlayerOutOfJail();

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has paid ' + outOfJailFee + ' to leave jail'));
        },
        useCard: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var player = state.getActivePlayer();

            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            if (state.getLastDiceRoll()) {
                return;
            }

            var card = player.getWallet().getCardById(data.id);

            if (!card) {
                return;
            }
            if ((card.isGetOutOfJail()) && (!state.isPlayerInJail(player))) {
                return;
            }

            state.tickActiveTimer();

            card.doAction(game.getBoard(), game.getState(), player);

            socket.emit('player-state', formatter.player.player(game, player));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has used the card "' + card.getText() + '"'));
        }
    },

    bind: function(server, user) {

        var _this = this;

        var socket = user.getSocket();

        socket.on('play-roll', function(data) {
            _this.on.roll(server, user, data);
        });

        socket.on('play-complete', function(data) {
            _this.on.complete(server, user, data);
        });

        socket.on('play-jail-pay', function(data) {
            _this.on.jailPay(server, user, data);
        });

        socket.on('play-card', function(data) {
            _this.on.useCard(server, user, data);
        });

        this.init(server, user, socket);
    },

    init: function(server, user, socket) {

    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
