var formatter = require('../formatters/formatter');

var events = {

    on: {
        create: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();

            var player = state.getActivePlayer();
            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var trade = state.getTrade();
            if (trade) {
                return;
            }

            trade = state.createTrade();

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            user.getSocket().emit('trade-state', formatter.trade.state(trade));
        },
        cancel: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();

            var player = state.getActivePlayer();
            if (!player) {
                return;
            }

            var trade = state.getTrade();
            if (!trade) {
                return;
            }

            var ownerPlayer = trade.getPlayer();
            var withPlayer = trade.getWithPlayer();

            // One of the two participants must be the active player
            var isOwnerPlayerActive = (ownerPlayer.getId() == player.getId());
            var isWithPlayerActive = ((withPlayer) && (withPlayer.getId() == player.getId()));
            if ((!isOwnerPlayerActive) && (!isWithPlayerActive)) {
                return;
            }

            // One of the two participants must be the user making the request
            var isOwnerPlayer = (ownerPlayer.getUser().getId() == user.getId());
            var isWithPlayer = ((withPlayer) && (withPlayer.getUser().getId() == user.getId()));
            if ((!isOwnerPlayer) && (!isWithPlayer)) {
                return;
            }

            state.clearTrade();

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));

            if (isOwnerPlayer) {
                if (withPlayer) {
                    withPlayer.getUser().getSocket().emit('trade-cancel', {});
                    withPlayer.getUser().getSocket().emit('player-state', formatter.player.player(game, withPlayer));
                    withPlayer.getUser().getSocket().emit('chat-message', formatter.chat.system(ownerPlayer.getUser().getName() + ' has cancelled the trade'));
                }

                ownerPlayer.getUser().getSocket().emit('trade-cancel', {});
                ownerPlayer.getUser().getSocket().emit('player-state', formatter.player.player(game, ownerPlayer));
                ownerPlayer.getUser().getSocket().emit('chat-message', formatter.chat.system('You have cancelled the trade'));
            }

            if (isWithPlayer) {
                ownerPlayer.getUser().getSocket().emit('trade-cancel', {});
                ownerPlayer.getUser().getSocket().emit('player-state', formatter.player.player(game, ownerPlayer));
                ownerPlayer.getUser().getSocket().emit('chat-message', formatter.chat.system(withPlayer.getUser().getName() + ' has declined the trade'));

                withPlayer.getUser().getSocket().emit('trade-cancel', {});
                withPlayer.getUser().getSocket().emit('player-state', formatter.player.player(game, withPlayer));
                withPlayer.getUser().getSocket().emit('chat-message', formatter.chat.system('You have declined the trade'));
            }
        },
        player: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();

            var player = state.getActivePlayer();
            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            var trade = state.getTrade();
            if ((!trade) || (trade.isOffered())) {
                return;
            }

            state.tickActiveTimer();

            if (data.id) {
                var withPlayer = state.getPlayerById(data.id);
                if (!withPlayer) {
                    return;
                }

                var currentWithPlayer = trade.getWithPlayer();
                if ((currentWithPlayer) && (currentWithPlayer.getId() != withPlayer.getId())) {
                    trade.removeWithPlayerDeeds();
                }

                trade.setWithPlayer(withPlayer);

                var balance = withPlayer.getWallet().getBalance();

                if (trade.getWithPlayerBalance() > balance) {
                    trade.setWithPlayerBalance(balance);
                }
            } else {
                trade.setWithPlayer(false);
            }

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            user.getSocket().emit('trade-state', formatter.trade.state(trade));
        },
        deed: function(server, user, data) {

            if (!data.id) {
                return;
            }

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var trade = state.getTrade();

            if (!trade) {
                var player = state.getActivePlayer();
                if ((!player) || (player.getUser().getId() != user.getId())) {
                    return;
                }

                trade = state.createTrade();
            }

            if (trade.isOffered()) {
                return;
            }

            var player = trade.getPlayer();
            if (player.getUser().getId() != user.getId()) {
                return;
            }

            var deed = board.getDeedDeck().getDeed(data.id);
            if (!deed) {
                return;
            }

            var deedPlayer = board.getDeedDeck().getDeedOwner(state, deed);
            if (!deedPlayer) {
                return;
            }

            if (trade.hasDeed(deed)) {
                return;
            }

            state.tickActiveTimer();

            if (deedPlayer.getId() == player.getId()) {
                trade.addPlayerDeed(deed);
            } else {

                var withPlayer = trade.getWithPlayer();
                if ((withPlayer) && (deedPlayer.getId() != withPlayer.getId())) {
                    return;
                }

                trade.setWithPlayer(deedPlayer);
                trade.addWithPlayerDeed(deed);
            }

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));

            user.getSocket().emit('trade-state', formatter.trade.state(trade));
            user.getSocket().emit('player-state', formatter.player.player(game, player));
        },
        removeDeed: function(server, user, data) {

            if (!data.id) {
                return;
            }

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            var trade = state.getTrade();

            if ((!trade) || (trade.isOffered())) {
                return;
            }

            var player = trade.getPlayer();
            if (player.getUser().getId() != user.getId()) {
                return;
            }

            var deed = board.getDeedDeck().getDeed(data.id);
            if (!deed) {
                return;
            }

            var deedPlayer = board.getDeedDeck().getDeedOwner(state, deed);
            if (!deedPlayer) {
                return;
            }

            state.tickActiveTimer();

            if (deedPlayer.getId() == player.getId()) {
                trade.removePlayerDeed(deed);
            } else {

                var withPlayer = trade.getWithPlayer();
                if ((withPlayer) && (deedPlayer.getId() != withPlayer.getId())) {
                    return;
                }

                trade.removeWithPlayerDeed(deed);
            }

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));

            user.getSocket().emit('trade-state', formatter.trade.state(trade));
            user.getSocket().emit('player-state', formatter.player.player(game, player));
        },
        balance: function(server, user, data) {

            data.player = data.player ? parseInt(data.player) : false;
            data.withPlayer = data.withPlayer ? parseInt(data.withPlayer) : false;

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var trade = state.getTrade();

            if ((!trade) || (trade.isOffered())) {
                return;
            }

            var player = trade.getPlayer();
            if (player.getUser().getId() != user.getId()) {
                return;
            }

            state.tickActiveTimer();

            if (data.player !== false) {
                var playerBalance = player.getWallet().getBalance();
                if (data.player > playerBalance) {
                    data.player = playerBalance;
                }

                trade.setPlayerBalance(data.player);
            }

            if (data.withPlayer !== false) {
                var withPlayer = trade.getWithPlayer();
                if (withPlayer) {
                    var withPlayerBalance = withPlayer.getWallet().getBalance();
                    if (data.withPlayer > withPlayerBalance) {
                        data.withPlayer = withPlayerBalance;
                    }

                }

                trade.setWithPlayerBalance(data.withPlayer);
            }

            user.getSocket().emit('trade-state', formatter.trade.state(trade));
        },
        offer: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var trade = state.getTrade();

            if ((!trade) || (trade.isOffered()) || (!trade.canOffer())) {
                return;
            }

            var player = trade.getPlayer();
            if (player.getUser().getId() != user.getId()) {
                return;
            }

            var withPlayer = trade.getWithPlayer();
            if (!withPlayer) {
                return;
            }

            state.setActiveTimer(withPlayer);
            trade.makeOffer();

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));

            user.getSocket().emit('trade-state', formatter.trade.state(trade));
            user.getSocket().emit('player-state', formatter.player.player(game, player));

            withPlayer.getUser().getSocket().emit('trade-state', formatter.trade.state(trade));
        },
        accept: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var trade = state.getTrade();

            if ((!trade) || (!trade.isOffered())) {
                return;
            }

            var player = trade.getWithPlayer();
            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            trade.accept();
            state.clearTrade();

            user.getSocket().emit('trade-accept', {});
            user.getSocket().emit('player-state', formatter.player.player(game, player));

            trade.getPlayer().getUser().getSocket().emit('trade-accept', {});
            trade.getPlayer().getUser().getSocket().emit('player-state', formatter.player.player(game, trade.getPlayer()));

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
        },
        counter: function(server, user, data) {

            var io = server.getIo();

            var game = server.getGameByUser(user);
            if (!game) {
                return;
            }

            var state = game.getState();
            var trade = state.getTrade();

            if ((!trade) || (!trade.isOffered())) {
                return;
            }

            var player = trade.getWithPlayer();
            if ((!player) || (player.getUser().getId() != user.getId())) {
                return;
            }

            trade = state.createCounterTrade();

            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));

            user.getSocket().emit('trade-state', formatter.trade.state(trade));
            user.getSocket().emit('player-state', formatter.player.player(game, player));

            trade.getWithPlayer().getUser().getSocket().emit('trade-state', formatter.trade.state(trade));
        }
    },

    bind: function(server, user) {

        var _this = this;

        var io = server.getIo();
        var socket = user.getSocket();

        socket.on('trade-create', function(data) {
            _this.on.create(server, user, data);
        });

        socket.on('trade-cancel', function(data) {
            _this.on.cancel(server, user, data);
        });

        socket.on('trade-player', function(data) {
            _this.on.player(server, user, data);
        });

        socket.on('trade-deed', function(data) {
            _this.on.deed(server, user, data);
        });

        socket.on('trade-deed-remove', function(data) {
            _this.on.removeDeed(server, user, data);
        });

        socket.on('trade-balance', function(data) {
            _this.on.balance(server, user, data);
        });

        socket.on('trade-offer', function(data) {
            _this.on.offer(server, user, data);
        });

        socket.on('trade-accept', function(data) {
            _this.on.accept(server, user, data);
        });

        socket.on('trade-counter', function(data) {
            _this.on.counter(server, user, data);
        });

        this.init(server, user, socket);
    },

    init: function(server, user, socket) {
    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
