var animals = require('animals');

var Game = require('../game');
var boardFactory = require('../board-factory');

var formatter = require('../formatters/formatter');

var events = {

    on: {
        create: function(server, user, data) {

            var game = server.getGameByUser(user);

            if (game) {
                return;
            }

            var io = server.getIo();
            var socket = user.getSocket();

            var board = data.board;

            if ((!board) || (!server.hasBoard(board))) {
                return;
            }

            var board = boardFactory.create(server, board);

            if (!board) {
                return;
            }

            var animal = animals();
            var name = 'Game ' + animal.charAt(0).toUpperCase() + animal.slice(1);
            var game = new Game(name, user, board);

            var state = game.getState();

            server.addGame(game);

            socket.join(game.getId());

            socket.emit('game-create', formatter.game.game(game));
            io.emit('game-list', formatter.game.games(server.getGames()));
            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system('Game created'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        },

        join: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGame(data.id);

            if (game.hasUser(user)) {
                return;
            }

            if (game.isBannedUser(user)) {
                return;
            }

            if(game) {
                game.addUser(user);
            }

            var state = game.getState();
            var board = game.getBoard();

            socket.join(game.getId());

            socket.emit('game-join', formatter.game.game(game));
            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has joined the game'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        },

        leave: function(server, user, data) {

            var io = server.getIo();
            var socket = user.getSocket();

            var game = server.getGameByUser(user);

            if (!game) {
                return;
            }

            var state = game.getState();
            var board = game.getBoard();

            game.removeUser(user);

            if (!game.hasUsers()) {
                server.removeGame(game);
            } else if (game.getHostUser().getId() == user.getId()) {
                game.reassignHostUser();
                io.to(game.getId()).emit('chat-message', formatter.chat.system(game.getHostUser().getName() + ' is now hosting this game'));
            }

            socket.leave(game.getId());

            socket.emit('game-leave', {});
            io.emit('game-list', formatter.game.games(server.getGames()));
            io.to(game.getId()).emit('game-user-list', formatter.game.users(game.getUsers(), game.getHostUser()));
            io.to(game.getId()).emit('player-list', formatter.player.players(game, state.getPlayers()));
            io.to(game.getId()).emit('token-list', formatter.token.tokens(board.getTokenBag().getAvailableTokens(state)));
            io.to(game.getId()).emit('chat-message', formatter.chat.system(user.getName() + ' has left the game'));
            io.to(game.getId()).emit('play-state', formatter.play.state(game, user));
        }
    },

    bind: function(server, user) {

        var _this = this;

        var socket = user.getSocket();

        socket.on('game-create', function(data) {
            _this.on.create(server, user, data);
        });

        socket.on('game-join', function(data) {
            _this.on.join(server, user, data);
        });

        socket.on('game-leave', function(data) {
            _this.on.leave(server, user, data);
        });

        user.onExpire(function(user) {
            _this.on.leave(server, user, {});
        });

        this.init(server, user, socket);
    },

    init: function(server, user, socket) {

        var game = server.getGameByUser(user);

        if (game) {
            socket.join(game.getId());
        }

        socket.emit('game-list', formatter.game.games(server.getGames()));
    }
};

module.exports = {
    bind: function(server, user) {
        events.bind(server, user);
    }
};
