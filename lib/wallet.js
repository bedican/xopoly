var Wallet = function(balance) {
    this.balance = balance;
    this.deeds = [];
    this.cards = [];
};

Wallet.prototype.export = function() {

    var deeds = [];
    for(var key in this.deeds) {
        deeds.push(this.deeds[key].export());
    }

    return {
        balance: this.balance,
        deeds: deeds
    };
};

Wallet.prototype.empty = function() {
    this.balance = 0;
    this.deeds = [];
    this.cards = [];
};

Wallet.prototype.getBalance = function() {
    return this.balance;
};

Wallet.prototype.addBalance = function(balance) {
    this.balance += balance;
};

Wallet.prototype.removeBalance = function(balance) {
    this.balance -= balance;

    if (this.balance < 0) {
        this.balance = 0;
    }
};

Wallet.prototype.hasBalance = function(balance) {
    return (this.balance >= balance);
};

Wallet.prototype.addCard = function(card) {
    this.cards.push(card);
};

Wallet.prototype.removeCard = function(card) {
    for (var key in this.cards) {
        if (this.cards[key].getId() == card.getId()) {
            this.cards.splice(key, 1);
            break;
        }
    }
};

Wallet.prototype.getCards = function() {
    return this.cards;
};

Wallet.prototype.getCardById = function(id) {
    for (var key in this.cards) {
        if (this.cards[key].getId() == id) {
            return this.cards[key];
        }
    }

    return false;
};

Wallet.prototype.getDeeds = function() {
    return this.deeds;
};

Wallet.prototype.addDeed = function(deed) {
    this.deeds.push(deed);
};

Wallet.prototype.hasDeed = function(deed) {
    for(var key in this.deeds) {
        if(this.deeds[key].getId() == deed.getId()) {
            return true;
        }
    }

    return false;
};

Wallet.prototype.removeDeed = function(deed) {
    for (var key in this.deeds) {
        if (this.deeds[key].getId() == deed.getId()) {
            this.deeds.splice(key, 1);
            break;
        }
    }
};

module.exports = Wallet;
