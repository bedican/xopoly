var idgen = require('./id-gen');

var Deed = function(set, name, price, image, housePrice, rent) {
    this.set = set;
    this.name = name;
    this.price = price;
    this.image = image;
    this.housePrice = housePrice || false;
    this.rent= rent || {};

    this.id = idgen.uniqueId();

    this.houses = 0;
    this.mortgaged = false;
};

Deed.prototype.export = function() {
    return {
        id: this.id,
        houses: this.houses,
        mortgaged: this.mortgaged
    };
};

Deed.prototype.init = function() {
    this.houses = 0;
    this.mortgaged = false;
};

Deed.prototype.getHousesPerHotel = function() {
    if (!this.isBuildable()) {
        return 0;
    }

    return 4;
};

Deed.prototype.getHouseLimit = function() {
    if (!this.isBuildable()) {
        return 0;
    }

    return (this.rent.property.length - 1);
};

Deed.prototype.getId = function() {
    return this.id;
};

Deed.prototype.getSet = function() {
    return this.set;
};

Deed.prototype.getName = function() {
    return this.name;
};

Deed.prototype.getPrice = function() {
    return this.price;
};

Deed.prototype.getMortgagePrice = function() {
    return Math.floor(this.price / 2);
};

Deed.prototype.getUnmortgagePrice = function() {
    var price = this.getMortgagePrice();
    var interest = Math.floor(price * 0.1);

    return price + interest;
};

Deed.prototype.getHousePrice = function() {
    return this.housePrice;
};

Deed.prototype.getHouseSalePrice = function() {
    return Math.floor(this.getHousePrice() / 2);
};

Deed.prototype.isBuildable = function() {
    if ((!this.rent.property) || (!this.rent.property.length)) {
        return false;
    }
    if (!this.housePrice) {
        return false;
    }

    return true;
};

Deed.prototype.getImage = function() {
    return this.image;
};

Deed.prototype.getHotels = function() {
    return Math.floor(this.houses / (this.getHousesPerHotel() + 1));
};

Deed.prototype.getHouses = function() {
    return this.houses % (this.getHousesPerHotel() + 1)
};

Deed.prototype.hasProperty = function() {
    return (this.houses > 0);
};

Deed.prototype.addHouse = function() {
    if (!this.isBuildable()) {
        return false;
    }
    if (this.isMortgaged()) {
        return false;
    }
    if (this.houses >= this.getHouseLimit()) {
        return false;
    }
    if((this.houses) && (this.houses % this.getHousesPerHotel() == 0)) {
        return false;
    }

    this.houses++;
    return this.getHouses();
};

Deed.prototype.removeHouse = function() {
    if (!this.isBuildable()) {
        return false;
    }
    if (!this.getHouses()) {
        return false;
    }

    this.houses--;
    return this.getHouses();
};

Deed.prototype.addHotel = function() {
    if (!this.isBuildable()) {
        return false;
    }
    if (this.isMortgaged()) {
        return false;
    }
    if (this.houses >= this.getHouseLimit()) {
        return false;
    }
    if((!this.houses) || (this.houses % this.getHousesPerHotel() != 0)) {
        return false;
    }

    this.houses++;
    return this.getHotels();
};

Deed.prototype.removeHotel = function() {
    if (!this.isBuildable()) {
        return false;
    }
    if (!this.getHotels()) {
        return false;
    }

    this.houses--;
    return this.getHotels();
};

Deed.prototype.isMortgaged = function() {
    return this.mortgaged;
};

Deed.prototype.mortgage = function() {
    this.mortgaged = true;
    return this.getMortgagePrice();
};

Deed.prototype.unmortgage = function() {
    this.mortgaged = false;
    return this.getUnmortgagePrice();
};

Deed.prototype.getPropertyRent = function() {

    if (this.isMortgaged()) {
        return 0;
    }

    if ((this.rent.property) && (this.rent.property.length)) {
        var index = (this.houses > this.getHouseLimit()) ? this.getHouseLimit() : this.houses;
        return this.rent.property[index];
    }

    return 0;
};

Deed.prototype.getDiceRollRent = function(roll, setCount) {

    if (this.isMortgaged()) {
        return 0;
    }

    if ((this.rent.dice) && (roll) && (roll.total)) {
        var index = ((setCount - 1) > (this.rent.dice.length - 1)) ? (this.rent.dice.length - 1) : (setCount - 1);
        return (this.rent.dice[index] * roll.total);
    }

    return 0;
};

Deed.prototype.getSetRent = function(setCount) {

    if (this.isMortgaged()) {
        return 0;
    }

    if ((this.rent.set) && (this.rent.set.length)) {
        var index = ((setCount - 1) > (this.rent.set.length - 1)) ? (this.rent.set.length - 1) : (setCount - 1);
        return this.rent.set[index];
    }

    return 0;
}

Deed.prototype.getSetDoubleRent = function() {

    if (this.isMortgaged()) {
        return false;
    }

    return this.rent['set-double'];
};

module.exports = Deed;
