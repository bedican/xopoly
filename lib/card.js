var idgen = require('./id-gen');
var actions = require('./card-actions');

var Card = function(set, text, action, params) {
    if(!actions[action]) {
        throw Error('Unknown card action ' + action);
    }

    this.set = set;
    this.text = text;
    this.action = action;
    this.params = params || {};

    this.id = idgen.uniqueId();
};

Card.prototype.export = function() {
    return {
        id: this.id
    };
};

Card.prototype.getId = function() {
    return this.id;
};

Card.prototype.getSet = function() {
    return this.set;
};

Card.prototype.getText = function() {
    return this.text;
};

Card.prototype.isRetainable = function() {
    return (this.action == 'get-out-jail');
};

Card.prototype.isGetOutOfJail = function() {
    return (this.action == 'get-out-jail');
};

Card.prototype.doAction = function(board, state, player) {
    return actions[this.action](board, state, player, this, this.params);
}

module.exports = Card;
