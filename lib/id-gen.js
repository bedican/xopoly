var crypto = require('crypto');

module.exports = {
    uniqueId: function() {
        return crypto
            .createHash('sha1')
            .update((new Date()).getTime() + Math.random().toString())
            .digest('hex');
    }
};
