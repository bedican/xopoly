var Express = require('express');
var SocketIo = require('socket.io');
var http = require('http');
var path = require('path');
var fs = require('fs');

var goby = require('goby').init({
    decorator: function(pieces) {
        return pieces.map(function(item) {
            return item.charAt(0).toUpperCase() + item.slice(1)
        }).join(' ');
    }
});

var User = require('./user');

var event = require('./events/event');
var formatter = require('./formatters/formatter');

var properties = {

    instance: (new Date()).getTime(),

    users: {},
    games: {},
    boards: {},

    app: null,
    server: null,
    io: null,
};

var methods = {

    init: function(server, socket, data) {

        var user;
        var sessionId = data.id;

        if ((sessionId) && (properties.users[sessionId])) {
            user = properties.users[sessionId];
        } else {
            user = new User(goby.generate(['adj', 'suf']));
            properties.users[user.getSessionId()] = user;
        }

        var userSocket = user.getSocket();

        // If there is a current socket and we have a new one, disconnect the old.
        if ((userSocket) && (userSocket != socket)) {
            userSocket.disconnect();
        }

        // If there is no current socket or we have a new one, bind events to it.
        if ((!userSocket) || (userSocket != socket)) {
            user.setSocket(socket);
            event.bind(server, user);
        }

        var gameData = false;
        var playStateData = false;
        var playerData = false;
        var tradeData = false;
        var playersData = false;
        var usersData = false;
        var tokenData = false;

        var game = server.getGameByUser(user);

        if (game) {
            var state = game.getState();
            var board = game.getBoard();
            var player = state.getPlayer(user);
            var trade = state.getTrade();

            gameData = formatter.game.game(game);
            playerData = player ? formatter.player.player(game, player) : false;
            playersData = formatter.player.players(game, state.getPlayers());
            usersData = formatter.game.users(game.getUsers(), game.getHostUser());
            tokenData = formatter.token.tokens(board.getTokenBag().getAvailableTokens(state));
            playStateData = formatter.play.state(game, user);
            tradeData = trade ? formatter.trade.state(trade) : false;
        }

        return {
            instance: properties.instance,
            id: user.getSessionId(),
            userId: user.getId(),
            username: user.getName(),
            boardList: formatter.board.boards(server.getBoards()),
            gameList: formatter.game.games(server.getGames()),
            game: gameData,
            playState: playStateData,
            player: playerData,
            playerList: playersData,
            userList: usersData,
            tokenList: tokenData,
            trade: tradeData
        };
    },

    disconnect: function(socket) {
        // The user should be removed when its session times out. not when it disconnects.
    },

    connection: function(server, socket) {

        var _this = this;

        socket.on('error', function(e) {
            console.error('Socket error: ' + e.message);
        });

        socket.on('disconnect', function() {
            _this.disconnect(socket);
        });

        socket.on('init', function(data) {
            socket.emit('init', _this.init(server, socket, data));
        });
    },

    initUserExpire: function(server) {

        var _this = this;

        setInterval(function() {
            _this.expireUsers(server);
        }, 20000);
    },

    expireUsers: function(server) {

        var _this = this;

        for(var key in properties.users) {
            var user = properties.users[key];
            if(!user.isConnected()) {
                properties.users[key].expire();
                delete properties.users[key];
            }
        }
    },

    initExport: function(server) {

        var _this = this;

        setInterval(function() {
            _this.exportServer(server);
        }, 30000);
    },

    exportServer: function(server) {
        // TODO... server.export() -> to file or db
        //console.log(JSON.stringify(server.export()));
    },

    addBoards: function(dir) {
        var dirs = fs.readdirSync(dir);

        for(var key in dirs) {
            if (this.hasBoard(dirs[key])) {
                continue;
            }

            var board = dir + '/' + dirs[key];
            if (fs.statSync(board).isDirectory()) {
                properties.boards[dirs[key]] = board;
            }
        }
    },

    hasBoard: function(board) {
        return properties.boards[board] ? true : false;
    },

    export: function() {

        var users = [];
        for(var key in properties.users) {
            users.push(properties.users[key].export());
        }

        var games = [];
        for(var key in properties.games) {
            games.push(properties.games[key].export());
        }

        return {
            instance: properties.instance,
            users: users,
            games: games
        };
    }
};

var server = {

    getInstance: function() {
        return properties.instance;
    },

    getIo: function() {
        return properties.io;
    },

    getApp: function() {
        return properties.app;
    },

    getGames: function() {
        var games = [];
        for(var key in properties.games) {
            games.push(properties.games[key]);
        }

        return games;
    },

    addGame: function(game) {
        properties.games[game.getId()] = game;
    },

    getGame: function(id) {
        return properties.games[id] || null;
    },

    getGameByUser: function(user) {
        for(var key in properties.games) {
            if (properties.games[key].hasUser(user)) {
                return properties.games[key];
            }
        }

        return null;
    },

    removeGame: function(game) {
        if (properties.games[game.getId()]) {
            delete properties.games[game.getId()];
        }
    },

    addBoards: function(dir) {
        methods.addBoards(dir);
    },

    getBoards: function() {
        return Object.keys(properties.boards);
    },

    getBoardPath: function(board) {
        return properties.boards[board] || false;
    },

    hasBoard: function(board) {
        return methods.hasBoard(board);
    },

    isUsername: function(username) {
        var exists = false;
        for(var key in properties.users) {
            exists = exists || (properties.users[key].getName().trim().toLowerCase() == username.trim().toLowerCase());
        }

        return exists;
    },

    isGameName: function(name) {
        var exists = false;
        for(var key in properties.games) {
            exists = exists || (properties.games[key].getName().trim().toLowerCase() == name.trim().toLowerCase());
        }

        return exists;
    },

    start: function() {

        var _this = this;

        if (!this.getBoards().length) {
            methods.addBoards(path.resolve(__dirname + '/../boards'));
        }

        properties.app = new Express();
        properties.server = http.Server(properties.app);
        properties.io = new SocketIo(properties.server);

        properties.app.use(Express.static(path.resolve(__dirname + '/../web')));

        for(var name in properties.boards) {
            properties.app.use('/boards/' + name, Express.static(properties.boards[name] + '/web'));
        }

        properties.io.on('connection', function(socket) {
            methods.connection(_this, socket);
        });

        methods.initUserExpire(this);
        methods.initExport(this);

        properties.server.listen(3000, function() {
            console.log('listening on *:3000');
        });
    },

    export: function() {
        return methods.export();
    }
};

module.exports = {
    start: function() {
        server.start();
    },
    addBoards: function(dir) {
        server.addBoards(dir);
    }
};
