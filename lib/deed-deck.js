var DeedDeck = function(deedPositions) {
    this.deedPositions = deedPositions;
    this.deeds = {};
};

DeedDeck.prototype.export = function() {

    var deeds = {};
    for(var set in this.deeds) {
        deeds[set] = {};
        for(var key in this.deeds[set]) {
            deeds[set][key] = this.deeds[set][key].getId();
        }
    }

    return {
        deeds: deeds
    };
};

DeedDeck.prototype.addDeed = function(deed) {
    var set = deed.getSet();

    if (!this.deedPositions[set]) {
        return;
    }

    if (!this.deeds[set]) {
        this.deeds[set] = [];
    }

    this.deeds[set].push(deed);
};

DeedDeck.prototype.getDeed = function(id) {
    for(var set in this.deeds) {
        for(var key in this.deeds[set]) {
            if (this.deeds[set][key].getId() == id) {
                return this.deeds[set][key];
            }
        }
    }

    return false;
};

DeedDeck.prototype.getDeedByPosition = function(position) {
    for(var set in this.deedPositions) {
        var index = this.deedPositions[set].indexOf(position);

        if (index == -1) {
            continue;
        }

        if ((!this.deeds[set]) || (!this.deeds[set][index])) {
            return false;
        }

        return this.deeds[set][index];
    }

    return false;
};

DeedDeck.prototype.getDeedsBySet = function(set) {
    if (!this.deeds[set]) {
        return false;
    }

    return this.deeds[set];
};

DeedDeck.prototype.getDeedOwner = function(state, deed) {

    if ((!state) || (!deed)) {
        return false;
    }

    var players = state.getPlayers();

    for(var key in players) {
        if (players[key].getWallet().hasDeed(deed)) {
            return players[key];
        }
    }

    return false;
};

DeedDeck.prototype.hasDeedSet = function(player, set) {
    var deeds = this.getDeedsBySet(set);

    if (!deeds.length) {
        return false;
    }

    var result = true;
    for(var key in deeds) {
        result = result && player.getWallet().hasDeed(deeds[key]);
    }

    return result;
};

DeedDeck.prototype.getDeedSetCount = function(player, set) {
    var deeds = this.getDeedsBySet(set);

    var count = 0;
    for(var key in deeds) {
        if (player.getWallet().hasDeed(deeds[key])) {
            count++;
        }
    }

    return count;
};

DeedDeck.prototype.getDeedRent = function(state, deed) {
    var player = this.getDeedOwner(state, deed);

    if (!player) {
        return 0;
    }

    var rent = 0;
    var setCount = this.getDeedSetCount(player, deed.getSet());

    rent += deed.getPropertyRent();
    rent += deed.getDiceRollRent(state.getLastDiceRoll(), setCount);
    rent += deed.getSetRent(setCount);

    if ((this.hasDeedSet(player, deed.getSet())) && (deed.getSetDoubleRent())) {
        rent *= 2;
    }

    return rent;
};

module.exports = DeedDeck;
