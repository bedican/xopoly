var Token = function(name, image) {
    this.name = name;
    this.image = image;
};

Token.prototype.getName = function() {
    return this.name;
};

Token.prototype.getImage = function() {
    return this.image;
};

module.exports = Token;
