module.exports = {
    balance: function(player) {
        return {
            balance: player.getWallet().getBalance(),
            debt: player.getDebt().getTotal()
        };
    },
    leave: function(player) {
        return {
            id: player.getId(),
            userId: player.getUser().getId()
        };
    },
    player: function(game, player) {

        var key;
        var deeds = {};
        var cards = {};

        var state = game.getState();
        var board = game.getBoard();

        var trade = state.getTrade();
        var playerDeeds = player.getWallet().getDeeds();

        for(key in playerDeeds) {
            var set = playerDeeds[key].getSet();

            if (!deeds[set]) {
                deeds[set] = [];
            }

            deeds[set].push({
                id: playerDeeds[key].getId(),
                name: playerDeeds[key].getName(),
                image: playerDeeds[key].getImage(),
                price: playerDeeds[key].getPrice(),
                houses: playerDeeds[key].getHouses(),
                hotels: playerDeeds[key].getHotels(),
                housePrice: playerDeeds[key].getHousePrice(),
                buildable: playerDeeds[key].isBuildable(),
                mortgaged: playerDeeds[key].isMortgaged(),
                mortgagePrice: playerDeeds[key].getMortgagePrice(),
                unmortgagePrice: playerDeeds[key].getUnmortgagePrice(),
                housesPerHotel: playerDeeds[key].getHousesPerHotel(),
                houseLimit: playerDeeds[key].getHouseLimit(),
                hasSet: board.getDeedDeck().hasDeedSet(player, playerDeeds[key].getSet()),
                rental: board.getDeedDeck().getDeedRent(state, playerDeeds[key]),
                inTrade: ((trade) && (trade.hasDeed(playerDeeds[key])))
            });
        }

        var playerCards = player.getWallet().getCards();

        for(key in playerCards) {
            var set = playerCards[key].getSet();

            if (!cards[set]) {
                cards[set] = [];
            }

            cards[set].push({
                id: playerCards[key].getId(),
                text: playerCards[key].getText(),
                outOfJail: playerCards[key].isGetOutOfJail()
            });
        }

        return {
            id: player.getId(),
            userId: player.getUser().getId(),
            token: player.getToken().getName(),
            tokenImage: player.getToken().getImage(),
            username: player.getUser().getName(),
            balance: player.getWallet().getBalance(),
            debt: player.getDebt().getTotal(),
            deeds: deeds,
            cards: cards
        };
    },
    players: function(game, players) {

        var list = [];
        for(var key in players) {
            list.push(this.player(game, players[key]));
        }

        return {
            players: list
        };
    }
};
