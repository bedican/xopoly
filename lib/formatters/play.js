module.exports = {
    state: function(game, user) {

        var state = game.getState();
        var board = game.getBoard();
        var player = state.getPlayer(user);

        var activePlayer = state.getActivePlayer();
        var lastDiceRoll = state.getLastDiceRoll();
        var deed = state.getLastDeed();
        var card = state.getLastCard();

        var deedPlayer = (deed) ? board.getDeedDeck().getDeedOwner(state, deed) : false;
        var playerPositions = state.getPlayerPositions();
        var deedPositions = board.getDeedPositions();

        var trade = state.getTrade();
        var lastTrade = state.getLastTrade();

        var deedData = false;
        var cardData = false;
        var tradeData = false;
        var deedPositionsData = [];

        // Last deed from roll
        if (deed) {
            deedData = {
                id: deed.getId(),
                name: deed.getName(),
                image: deed.getImage(),
                price: deed.getPrice(),
                houses: deed.getHouses(),
                hotels: deed.getHotels(),
                buildable: deed.isBuildable(),
                housePrice: deed.getHousePrice(),
                hasSet: (deedPlayer) ? board.getDeedDeck().hasDeedSet(deedPlayer, deed.getSet()) : false,
                rental: (deedPlayer) ? board.getDeedDeck().getDeedRent(state, deed) : false,
                player: (deedPlayer) ? deedPlayer.getId() : false,
                playerName: (deedPlayer) ? deedPlayer.getUser().getName() : false,
                traded: ((lastTrade) && (lastTrade.hasPlayerDeed(deed)))
            };
        }

        // Last card from roll
        if (card) {
            cardData = {
                text: card.getText(),
                retainable: card.isRetainable()
            };
        }

        // Current trade
        if (trade) {
            tradeData = {
                player: trade.getPlayer().getId(),
                withPlayer: (trade.getWithPlayer()) ? trade.getWithPlayer().getId() : false,
                offered: trade.isOffered()
            };
        }

        // Deed positions on the board
        for(var key in deedPositions) {
            deed = board.getDeedDeck().getDeedByPosition(deedPositions[key]);
            deedPlayer = board.getDeedDeck().getDeedOwner(state, deed);

            deedPositionsData.push({
                position: deedPositions[key],
                id: deed.getId(),
                name: deed.getName(),
                image: deed.getImage(),
                price: deed.getPrice(),
                houses: deed.getHouses(),
                hotels: deed.getHotels(),
                housePrice: deed.getHousePrice(),
                mortgaged: deed.isMortgaged(),
                rental: board.getDeedDeck().getDeedRent(state, deed),
                player: (deedPlayer) ? deedPlayer.getId() : false,
                playerName: (deedPlayer) ? deedPlayer.getUser().getName() : false,
                inTrade: ((trade) && (trade.hasDeed(deed)))
            });
        }

        return {
            user: (activePlayer) ? activePlayer.getUser().getId() : false,
            player: (activePlayer) ? activePlayer.getId() : false,
            playerName: (activePlayer) ? activePlayer.getUser().getName() : false,
            playerDebt: (activePlayer) ? activePlayer.getDebt().getTotal() : 0,
            playerBalance: (activePlayer) ? activePlayer.getWallet().getBalance() : 0,
            playerInJailRolls: (activePlayer) ? state.getPlayerJailRolls(activePlayer) : false,
            outOfjailFee: game.getBoard().getConfig().getOutOfJailFee(),
            roll: (lastDiceRoll) ? lastDiceRoll : false,
            playerPositions: playerPositions,
            deedPositions: deedPositionsData,
            deed: deedData,
            card: cardData,
            trade: tradeData
        }
    }
};
