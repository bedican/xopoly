module.exports = {
    tokens: function(tokens) {
        var list = [];

        for(var key in tokens) {
            list.push({
                name: tokens[key].getName(),
                image: tokens[key].getImage()
            });
        }

        return {
            tokens: list
        };
    }
};
