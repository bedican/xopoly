module.exports = {
    game: function(game) {
        return {
            id: game.getId(),
            name: game.getName(),
            hostUsername: game.getHostUser().getName(),
            theme: '/boards/' + game.getBoard().getName() + '/css/board.css'
        };
    },
    games: function(games) {
        var list = [];

        for(var key in games) {
            list.push(this.game(games[key]));
        }

        return {
            games: list
        };
    },
    users: function(users, hostUser) {
        var list = [];

        for(var key in users) {
            list.push({
                id: users[key].getId(),
                name: users[key].getName()
            });
        }

        return {
            host: hostUser.getId(),
            users: list
        };
    }
};
