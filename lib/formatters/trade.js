module.exports = {
    state: function(trade) {

        var player = trade.getPlayer();
        var withPlayer = trade.getWithPlayer();

        var deed;

        var playerDeedsData = [];
        var playerDeeds = trade.getPlayerDeeds();

        for(var key in playerDeeds) {
            deed = playerDeeds[key];
            playerDeedsData.push({
                id: deed.getId(),
                image: deed.getImage(),
                name: deed.getName()
            });
        }

        var withPlayerDeedsData = [];
        var withPlayerDeeds = trade.getWithPlayerDeeds();

        for(var key in withPlayerDeeds) {
            deed = withPlayerDeeds[key];
            withPlayerDeedsData.push({
                id: deed.getId(),
                image: deed.getImage(),
                name: deed.getName()
            });
        }

        return {
            player: player.getId(),
            playerName: player.getUser().getName(),
            withPlayer: (withPlayer) ? withPlayer.getId() : false,
            withPlayerName: (withPlayer) ? withPlayer.getUser().getName() : false,
            playerDeeds: playerDeedsData,
            withPlayerDeeds: withPlayerDeedsData,
            playerBalance: trade.getPlayerBalance(),
            withPlayerBalance: trade.getWithPlayerBalance(),
            canOffer: trade.canOffer(),
            isOffered: trade.isOffered()
        };
    }
};
