module.exports = {
    game: require('../formatters/game'),
    player: require('../formatters/player'),
    token: require('../formatters/token'),
    board: require('../formatters/board'),
    chat: require('../formatters/chat'),
    play: require('../formatters/play'),
    trade: require('../formatters/trade')
};
