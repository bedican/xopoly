module.exports = {
    message: function(user, message) {
        return {
            id: user.getId(),
            username: user.getName(),
            messages: message ? [ message ] : false
        };
    },
    system: function(message) {
        return this.multi([ message ]);
    },
    multi: function(messages) {
        return {
            messages: messages
        };
    }
};
