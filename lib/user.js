var idgen = require('./id-gen');

var User = function(name) {
    this.name = name;

    this.socket = null;
    this.expireHandlers = [];

    this.id = idgen.uniqueId();
    this.sessionId = idgen.uniqueId();
};

User.prototype.export = function() {
    return {
        name: this.name,
        id: this.id,
        sessionId: this.sessionId
    };
};

User.prototype.getId = function() {
    return this.id;
};

User.prototype.getSessionId = function() {
    return this.sessionId;
};

User.prototype.getName = function() {
    return this.name;
};

User.prototype.setName = function(name) {
    this.name = name;
};

User.prototype.getSocket = function() {
    return this.socket;
};

User.prototype.getSocketId = function() {
    return this.socket.id;
};

User.prototype.isConnected = function() {
    return ((this.socket) && (this.socket.connected));
};

User.prototype.getClientIp = function() {
    return this.socket.request.connection.remoteAddress;
};

User.prototype.setSocket = function(socket) {
    this.socket = socket;
};

User.prototype.onExpire = function(handler) {
    this.expireHandlers.push(handler);
};

User.prototype.expire = function() {
    for(var key in this.expireHandlers) {
        this.expireHandlers[key](this);
    }
};

module.exports = User;
