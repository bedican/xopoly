module.exports = {
    'collect-from-bank': function (board, state, player, card, params) {
        if(!params.amount) {
            return;
        }

        player.getWallet().addBalance(params.amount);
    },
    'collect-from-players': function (board, state, player, card, params) {
        if(!params.amount) {
            return;
        }

        var amount = 0;
        var players = state.getPlayers();

        for(var key in players) {
            if(player.getId() != players[key].getId()) {
                if (players[key].getWallet().hasBalance(params.amount)) {
                    amount += params.amount;
                    players[key].getWallet().removeBalance(params.amount);
                } else {
                    players[key].getDebt().addPlayerDebt(player, params.amount);
                }
            }
        }

        player.getWallet().addBalance(amount);
    },
    'pay-to-players': function (board, state, player, card, params) {
        if(!params.amount) {
            return;
        }

        var players = state.getPlayers();
        var amount = params.amount * (players.length - 1);

        if (player.getWallet().hasBalance(amount)) {
            for(var key in players) {
                if(player.getId() != players[key].getId()) {
                    players[key].getWallet().addBalance(params.amount);
                }
            }

            player.getWallet().removeBalance(amount);

        } else {
            for(var key in players) {
                if(player.getId() != players[key].getId()) {
                    player.getDebt().addPlayerDebt(players[key], params.amount);
                }
            }
        }
    },
    'go-to-position': function (board, state, player, card, params) {

        var passGo = !params['skip-pass-go'];

        var playerPosition = state.getPlayerPosition(player);
        var hasMoved = state.movePlayerToPosition(params.position);

        if ((playerPosition > params.position) && (passGo) && (hasMoved)) {
            board.doPassGoAction(state, player);
        }
    },
    'building-fees': function (board, state, player, card, params) {
        if((!params['amount-hotel']) || (!params['amount-house'])) {
            return;
        }

        var balance = 0;
        var deeds = player.getWallet().getDeeds();

        for(var key in deeds) {
            balance += (parseInt(params['amount-hotel']) * deeds[key].getHotels());
            balance += (parseInt(params['amount-house']) * deeds[key].getHouses());
        }

        if (player.getWallet().hasBalance(balance)) {
            player.getWallet().removeBalance(balance);
        } else {
            player.getDebt().addDebt(balance);
        }
    },
    'pay-to-bank': function (board, state, player, card, params) {
        if(!params.amount) {
            return;
        }

        if (player.getWallet().hasBalance(params.amount)) {
            player.getWallet().removeBalance(params.amount);
        } else {
            player.getDebt().addDebt(params.amount);
        }
    },
    'go-to-jail': function (board, state, player, card, params) {
        state.movePlayerToJail();
    },
    'get-out-jail': function (board, state, player, card, params) {
        state.movePlayerOutOfJail();
        player.getWallet().removeCard(card);
        board.getCardDeck().addCard(card);
    }
};
