var path = require('path');
var fs = require('fs');

var Board = require('./board');
var Deed = require('./deed');
var Token = require('./token');
var Card = require('./card');

module.exports = {
    create: function(server, board) {

        var boardPath = server.getBoardPath(board);

        if (!boardPath) {
            return false;
        }

        try {
            var filename = boardPath +'/board.json';
            var boardData = fs.readFileSync(filename, 'utf8');
            var config = JSON.parse(boardData);
        } catch (e) {
            throw new Error('Failed to load board "' + board + '" definition file "' + filename + '"');
        }

        var board = new Board(config.name, config.description, config.config);

        for(var setName in config.deeds) {
            var set = config.deeds[setName];
            for(var key in set) {
                var deed = set[key];
                board.getDeedDeck().addDeed(new Deed(setName, deed.name, deed.price, deed.image, deed['house-price'], deed.rent));
            }
        }

        for(var setName in config.cards) {
            var set = config.cards[setName];
            for(var key in set) {
                var card = set[key];
                board.getCardDeck().addCard(new Card(setName, card.text, card.action, card.params));
            }
        }

        for(var key in config.tokens) {
            var token = config.tokens[key];
            board.getTokenBag().addToken(new Token(token.name, token.image));
        }

        return board;
    }
};
