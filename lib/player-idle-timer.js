var PlayerIdleTimer = function (player, seconds) {
    this.player = player;
    this.seconds = seconds;

    this.handlers = [];
    this.idleTimeout = false;
};

PlayerIdleTimer.prototype.tick = function() {

    var _this = this;

    this.clear();

    this.idleTimeout = setTimeout(function() {
        _this.timeout();
    }, this.seconds * 1000);
};

PlayerIdleTimer.prototype.onTimeout = function(handler) {
    this.handlers.push(handler);
};

PlayerIdleTimer.prototype.timeout = function() {
    for(var key in this.handlers) {
        this.handlers[key](this.player);
    }
};

PlayerIdleTimer.prototype.clear = function() {
    if (this.idleTimeout) {
        clearTimeout(this.idleTimeout);
    }
};

module.exports = PlayerIdleTimer;
