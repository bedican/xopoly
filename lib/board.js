var BoardConfig = require('./board-config');
var TokenBag = require('./token-bag');
var DeedDeck = require('./deed-deck');
var CardDeck = require('./card-deck');

var actions = require('./board-actions');

var Board = function(name, description, config) {

    this.name = name;
    this.description = description;

    this.config = new BoardConfig(config);

    this.namedPositions = {
        go: 1,
        first: 1,
        last: 40,
        jail: 31,
        visiting: 11
    }

    this.deedPositions = {
        'stations': [6, 16, 26, 36],
        'utilities': [13, 29],
        'brown': [2, 4],
        'light-blue': [7, 9, 10],
        'pink': [12, 14, 15],
        'orange': [17, 19, 20],
        'red': [22, 24, 25],
        'yellow': [27, 28, 30],
        'green': [32, 33, 35],
        'dark-blue': [38, 40]
    };

    this.actionPositions = {
        'deed': [2, 4, 6, 7, 9, 10, 12, 13, 14, 15, 16, 17, 19, 20, 22, 24, 25, 26, 27, 28, 29, 30, 32, 33, 35, 36, 38, 40],
        'income-tax': [5],
        'super-tax': [39],
        'community-chest': [3, 18, 34],
        'chance': [8, 23, 37],
        'go-to-jail': [31]
    };

    this.deedDeck = new DeedDeck(this.deedPositions);
    this.cardDeck = new CardDeck();
    this.tokenBag = new TokenBag();
};

Board.prototype.export = function() {
    return {
        name: this.name,
        deedDeck: this.deedDeck.export()
    };
};

Board.prototype.getName = function() {
    return this.name;
};

Board.prototype.getDescription = function() {
    return this.description;
};

Board.prototype.getConfig = function() {
    return this.config;
};

Board.prototype.getNamedPositions = function() {
    return this.namedPositions;
};

Board.prototype.doAction = function(position, state, player) {
    for(var key in this.actionPositions) {
        var index = this.actionPositions[key].indexOf(position);

        if (index == -1) {
            continue;
        }

        if (!actions[key]) {
            return false;
        }

        return actions[key](this, state, player);
    }

    return false;
}

Board.prototype.isDeedAction = function(position) {
    return (this.actionPositions['deed'].indexOf(position) != -1);
};

Board.prototype.isCardAction = function(position) {
    return (
        (this.actionPositions['chance'].indexOf(position) != -1) ||
        (this.actionPositions['community-chest'].indexOf(position) != -1)
    );
};

Board.prototype.doPassGoAction = function(state, player) {
    actions['pass-go'](this, state, player);
};

Board.prototype.getDeedPositions = function() {
    var positions = [];

    for(var key in this.deedPositions) {
        positions = positions.concat(this.deedPositions[key]);
    }

    return positions;
};

Board.prototype.getDeedDeck = function() {
    return this.deedDeck;
};

Board.prototype.getCardDeck = function() {
    return this.cardDeck;
};

Board.prototype.getTokenBag = function() {
    return this.tokenBag;
};

module.exports = Board;
