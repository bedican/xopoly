var shuffle = require('knuth-shuffle').knuthShuffle;

var CardDeck = function() {
    this.cards = {
        'chance': [],
        'community-chest': []
    };
};

CardDeck.prototype.addCard = function(card) {
    var set = card.getSet();

    if (!this.cards[set]) {
        return;
    }

    this.cards[set].push(card);
};

CardDeck.prototype.drawCard = function(set) {
    if (!this.cards[set]) {
        return false;
    }

    return this.cards[set].shift();
};

CardDeck.prototype.shuffle = function() {
    for (var set in this.cards) {
        shuffle(this.cards[set]);
    }
};

module.exports = CardDeck;
