var TokenBag = function() {
    this.tokens = [];
};

TokenBag.prototype.addToken = function(token) {
    this.tokens.push(token);
};

TokenBag.prototype.getToken = function(name) {
    for(var key in this.tokens) {
        if(this.tokens[key].getName() == name) {
            return this.tokens[key];
        }
    }

    return false;
};

TokenBag.prototype.getAvailableTokens = function(state) {

    var tokens = [];
    var players = state.getPlayers();

    for(var tokenKey in this.tokens) {

        var found = false;
        var token = this.tokens[tokenKey];

        for(var playerKey in players) {
            found = found || (players[playerKey].getToken().getName() == token.getName());
        }

        if (!found) {
            tokens.push(token);
        }
    }

    return tokens;
};

TokenBag.prototype.hasAvailableToken = function(state, token) {

    var tokenNames = [];
    var tokens = this.getAvailableTokens(state);

    for(var key in tokens) {
        tokenNames.push(tokens[key].getName());
    }

    return (tokenNames.indexOf(token.getName()) != -1);
};

module.exports = TokenBag;
