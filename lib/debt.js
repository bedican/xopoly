var Debt = function(player) {
    this.player = player;

    this.bank = 0;
    this.players = [];
}

Debt.prototype.export = function() {

    var players = {};
    for(var key in this.players) {
        players[this.players[key].player.getId()] = this.players[key].balance;
    }

    return {
        player: this.player.getId(),
        bank: this.bank,
        players: players
    };
};

Debt.prototype.addPlayerDebt = function(player, balance) {
    this.players.push({
        player: player,
        balance: balance
    });
};

Debt.prototype.removePlayerDebt = function(player) {

    var keys = [];
    for(var key in this.players) {
        if (this.players[key].player.getId() == player.getId()) {
            keys.push(key);
        }
    }

    for(var key in keys) {
        this.players.splice(keys[key], 1);
    }
};

Debt.prototype.addDebt = function(balance) {
    this.bank += balance;
};

Debt.prototype.getTotal = function() {

    var total = this.bank;

    for(var key in this.players) {
        total += this.players[key].balance;
    }

    return total;
};

Debt.prototype.canSettle = function() {
    return this.player.getWallet().hasBalance(this.getTotal());
};

Debt.prototype.settle = function() {

    if (!this.canSettle()) {
        return;
    }

    this.player.getWallet().removeBalance(this.bank);

    for(var key in this.players) {
        this.player.getWallet().removeBalance(this.players[key].balance);
        this.players[key].player.getWallet().addBalance(this.players[key].balance);
    }

    this.clear();
};

Debt.prototype.clear = function() {
    this.bank = 0;
    this.players = [];
};

Debt.prototype.isOutstanding = function() {
    return ((this.bank > 0) || (this.players.length > 0));
};

module.exports = Debt;
