var PlayerIdleTimer = require('./player-idle-timer');
var Trade = require('./trade');

var dice = require('./dice');

var GameState = function(board) {
    this.board = board;

    this.players = [];

    this.activePlayer = 0;
    this.playerPositions = {};
    this.playerTimers = {};
    this.activeTimer = false;

    this.lastDiceRoll = false;
    this.lastDeed = false;
    this.lastCard = false;

    this.playerJailRolls = {};
    this.playerDoubleRolls = {};

    this.trade = false;
    this.lastTrade = false;

    this.positions = board.getNamedPositions();

    this.board.getCardDeck().shuffle();
};

GameState.prototype.export = function() {

    var players = [];
    for(var key in this.players) {
        players.push(this.players[key].export());
    }

    return {
        players: players,
        active: this.activePlayer
    };
};

GameState.prototype.getLastDiceRoll = function() {
    return this.lastDiceRoll;
};

GameState.prototype.getLastDeed = function() {
    return this.lastDeed;
};

GameState.prototype.getLastCard = function() {
    return this.lastCard;
};

GameState.prototype.getTrade = function() {
    return this.trade;
};

GameState.prototype.createTrade = function() {

    this.trade = new Trade(this.getActivePlayer());
    this.setActiveTimer(this.getActivePlayer());

    return this.trade;
};

GameState.prototype.createCounterTrade = function() {

    this.trade = this.trade.counter();
    this.setActiveTimer(this.trade.getPlayer());

    return this.trade;
};

GameState.prototype.clearTrade = function() {

    this.lastTrade = this.trade.isAccepted() ? this.trade : false;
    this.trade = false;

    this.setActiveTimer(this.getActivePlayer());
};

GameState.prototype.getLastTrade = function() {
    return this.lastTrade;
};

GameState.prototype.getPlayer = function(user) {
    for(var key in this.players) {
        if (this.players[key].getUser().getId() == user.getId()) {
            return this.players[key];
        }
    }

    return false;
};

GameState.prototype.getPlayerById = function(id) {
    for(var key in this.players) {
        if (this.players[key].getId() == id) {
            return this.players[key];
        }
    }

    return false;
};

GameState.prototype.getPlayers = function() {
    return this.players;
};

GameState.prototype.getActivePlayer = function() {
    if (!this.players[this.activePlayer]) {
        return false;
    }

    return this.players[this.activePlayer];
};

GameState.prototype.addPlayer = function(player) {

    var _this = this;

    if (!this.board.getTokenBag().hasAvailableToken(this, player.getToken())) {
        return;
    }

    var timer = new PlayerIdleTimer(player, this.board.getConfig().getPlayerTimeout());

    timer.onTimeout(function(player) {
        _this.removePlayer(player.getUser());
    });

    this.players.push(player);

    this.playerTimers[player.getId()] = timer;
    this.playerJailRolls[player.getId()] = 0;
    this.playerDoubleRolls[player.getId()] = 0;
    this.playerPositions[player.getId()] = this.positions.first;

    if (this.getActivePlayer().getId() == player.getId()) {
        this.setActiveTimer(player);
    }

    return true;
};

GameState.prototype.removePlayer = function(user) {

    var playerKey = false

    for(var key in this.players) {
        if (this.players[key].getUser().getId() == user.getId()) {
            playerKey = key;
            break;
        }
    }

    if (!this.players[playerKey]) {
        return;
    }

    var deeds, cards;
    var player = this.players[playerKey];

    if ((this.trade) && (this.trade.isParticipant(player))) {
        this.trade = false;
    }

    deeds = player.getWallet().getDeeds();
    for (var key in deeds) {
        deeds[key].init();
    }

    cards = player.getWallet().getCards();
    for (var key in cards) {
        this.board.getCardDeck().addCard(cards[key]);
    }

    player.getWallet().empty();

    for(var key in this.players) {
        this.players[key].getDebt().removePlayerDebt(player);
    }

    this.players.splice(playerKey, 1);

    var wasActive = false;
    if (playerKey == this.activePlayer) {

        wasActive = true;
        this.lastDiceRoll = false;
        this.lastDeed = false;
        this.lastCard = false;

    } else if (playerKey < this.activePlayer) {
        this.activePlayer--;
    }

    if (this.activePlayer == this.players.length) {
        this.activePlayer = 0;
    }

    if ((wasActive) && (this.players.length)) {
        this.setActiveTimer(this.getActivePlayer());
    }

    this.playerTimers[player.getId()].clear();

    delete this.playerJailRolls[player.getId()];
    delete this.playerDoubleRolls[player.getId()];
    delete this.playerPositions[player.getId()];
    delete this.playerTimers[player.getId()];
};

GameState.prototype.getPlayerPosition = function(player) {

    if (!player) {
        return false;
    }

    var id = player.getId();

    if (this.playerPositions[id]) {
        return this.playerPositions[id];
    }

    return false;
};

GameState.prototype.getPlayerPositions = function() {
    return this.playerPositions;
};

GameState.prototype.isPlayerInJail = function(player) {
    return this.getPlayerPosition(player) == this.positions.jail;
};

GameState.prototype.getPlayerJailRolls = function(player) {
    return this.playerJailRolls[player.getId()] || 0;
};

GameState.prototype.getPlayerDoubleRolls = function(player) {
    return this.playerDoubleRolls[player.getId()] || 0;
};

GameState.prototype.getPlayerTimer = function(player) {
    return this.playerTimers[player.getId()];
};

GameState.prototype.tickActiveTimer = function() {
    if (!this.activeTimer) {
        return;
    }
    if (!this.playerTimers[this.activeTimer]) {
        return;
    }

    this.playerTimers[this.activeTimer].tick();
};

GameState.prototype.setActiveTimer = function(player) {

    var id = player.getId();

    if (!this.playerTimers[id]) {
        return;
    }

    if ((this.activeTimer) && (this.playerTimers[this.activeTimer])) {
        this.playerTimers[this.activeTimer].clear();
    }

    this.activeTimer = id;
    this.playerTimers[id].tick();
};

GameState.prototype.rollDice = function() {

    var player = this.getActivePlayer();

    if (!player) {
        return false;
    }

    this.tickActiveTimer();

    var position, action;
    var id = player.getId();

    // Clear last deed / card
    this.lastDeed = false;
    this.lastCard = false;

    // Roll the dice
    this.lastDiceRoll = dice.rollDouble();

    // Is player in jail ?
    if (this.isPlayerInJail(player)) {

        // If not the third roll, remain in jail.
        if (this.playerJailRolls[id] > 0) {
            this.playerJailRolls[id]--;

            // Unless a double is rolled, move to just visiting
            // The player does not get to roll again, and the double is used up so does not move
            if (this.lastDiceRoll.double) {
                this.movePlayerOutOfJail();
            }

            // If the roll before the third, move to just visiting,
            // and pay fine in preperation for the next roll
            if (this.playerJailRolls[id] == 0) {
                var outOfJailFee = this.board.getConfig().getOutOfJailFee();

                if (player.getWallet().hasBalance(outOfJailFee)) {
                    player.getWallet().removeBalance(outOfJailFee);
                } else {
                    player.getDebt().addDebt(outOfJailFee);
                }

                this.movePlayerOutOfJail();
            }

            return this.lastDiceRoll;
        }
    }

    // Was the last roll a double? keep a tally.
    if (this.lastDiceRoll.double) {
        this.playerDoubleRolls[id]++;
    } else {
        this.playerDoubleRolls[id] = 0;
    }

    // If third roll of double, dont action the result, and go to jail.
    if (this.playerDoubleRolls[id] >= this.board.getConfig().getDoubleRollsToJail()) {
        this.movePlayerToJail();
        this.playerDoubleRolls[id] = 0;
        return this.lastDiceRoll;
    }

    // Move the player
    position = this.playerPositions[id] + this.lastDiceRoll.total;

    // Pass go ?
    if (position > this.positions.last) {
        position = (this.positions.first - 1) + (position - this.positions.last);
        this.board.doPassGoAction(this, player);
    }

    // Store position
    this.playerPositions[id] = position;

    // Run the action on the position we landed on
    var result = this.board.doAction(position, this, player);

    // store the result of the action if needed
    if (this.board.isCardAction(position)) {
        this.lastCard = result;
    } else if (this.board.isDeedAction(position)) {
        this.lastDeed = result;
    }

    return this.lastDiceRoll;
};

GameState.prototype.nextPlayer = function() {

    var player = this.getActivePlayer();

    if (!player) {
        this.lastDiceRoll = false;
        return;
    }

    if (this.trade) {
        return;
    }

    // If the player has incurred debt that cannot be paid,
    // they are now bankrupt and should be removed.
    var debt = player.getDebt();

    if (debt.isOutstanding()) {
        if (debt.canSettle()) {
            debt.settle();
        } else {
            // Remove player will have handled
            // progressing to the next player.
            this.removePlayer(player.getUser());
            return;
        }
    }

    // if
    // - last roll was double
    // - we are below the double roll limit
    // - we are not in jail
    // then do not move to next player

    if (
        (this.lastDiceRoll.double) &&
        (this.playerDoubleRolls[player.getId()] < this.board.getConfig().getDoubleRollsToJail()) &&
        (!this.isPlayerInJail(player))
    ) {
        this.tickActiveTimer();
        this.lastDiceRoll = false;
        return;
    }

    if (this.activePlayer < (this.players.length - 1)) {
        this.activePlayer++;
    } else {
        this.activePlayer = 0;
    }

    this.setActiveTimer(this.getActivePlayer());

    this.lastDiceRoll = false;
    this.lastTrade = false;
};

GameState.prototype.movePlayerToPosition = function(position) {

    var player = this.getActivePlayer();

    if (!player) {
        return false;
    }

    if ((position < this.positions.first) || (position > this.positions.last)) {
        return false;
    }

    if(position == this.positions.first) {
        this.board.doPassGoAction(this, player);
    }

    this.playerPositions[player.getId()] = position;

    return true;
};

GameState.prototype.movePlayerOutOfJail = function() {

    var player = this.getActivePlayer();

    if (!player) {
        return;
    }

    var id = player.getId();

    this.playerJailRolls[id] = 0;

    if (this.playerPositions[id]) {
        this.playerPositions[id] = this.positions.visiting;
    }
};

GameState.prototype.movePlayerToJail = function() {

    var player = this.getActivePlayer();

    if (!player) {
        return;
    }

    var id = player.getId();

    this.playerJailRolls[id] = 2;
    this.playerDoubleRolls[id] = 0;

    if (this.playerPositions[id]) {
        this.playerPositions[id] = this.positions.jail;
    }
};

module.exports = GameState;
