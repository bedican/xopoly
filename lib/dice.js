module.exports = {
    roll: function() {
        return Math.floor(Math.random() * (6 - 2) + 1);
    },
    rollDouble: function() {

        var first = this.roll();
        var second = this.roll();

        return {
            first: first,
            second: second,
            total: first + second,
            double: (first == second)
        };
    }
};
