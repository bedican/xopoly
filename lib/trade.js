var Trade = function(player) {
    this.player = player;

    this.offered = false;
    this.accepted = false;

    this.withPlayer = false;

    this.playerDeeds = [];
    this.withPlayerDeeds = [];

    this.playerBalance = 0;
    this.withPlayerBalance = 0;
};

Trade.prototype.getPlayer = function() {
    return this.player;
};

Trade.prototype.getWithPlayer = function() {
    return this.withPlayer;
};

Trade.prototype.setWithPlayer = function(player) {
    this.withPlayer = player;

    if (!this.withPlayer) {
        this.removeWithPlayerDeeds();
    }
};

Trade.prototype.isParticipant = function(player) {
    return (
        (this.player.getId() == player.getId()) ||
        ((this.withPlayer) && (this.withPlayer.getId() == player.getId()))
    );
};

Trade.prototype.hasDeed = function(deed) {
    return ((this.hasPlayerDeed(deed)) || (this.hasWithPlayerDeed(deed)));
};

Trade.prototype.getPlayerDeeds = function() {
    return this.playerDeeds;
};

Trade.prototype.hasPlayerDeed = function(deed) {
    for(var key in this.playerDeeds) {
        if (this.playerDeeds[key].getId() == deed.getId()) {
            return true;
        }
    }

    return false;
};

Trade.prototype.addPlayerDeed = function(deed) {
    this.playerDeeds.push(deed);
};

Trade.prototype.removePlayerDeed = function(deed) {
    var index = this.playerDeeds.indexOf(deed);

    if (index != -1) {
        this.playerDeeds.splice(index, 1);
    }
};

Trade.prototype.getWithPlayerDeeds = function() {
    return this.withPlayerDeeds;
};

Trade.prototype.hasWithPlayerDeed = function(deed) {
    for(var key in this.withPlayerDeeds) {
        if (this.withPlayerDeeds[key].getId() == deed.getId()) {
            return true;
        }
    }

    return false;
};

Trade.prototype.addWithPlayerDeed = function(deed) {
    this.withPlayerDeeds.push(deed);
};

Trade.prototype.removeWithPlayerDeed = function(deed) {
    var index = this.withPlayerDeeds.indexOf(deed);

    if (index != -1) {
        this.withPlayerDeeds.splice(index, 1);
    }
};

Trade.prototype.removeWithPlayerDeeds = function() {
    this.withPlayerDeeds = [];
};

Trade.prototype.getPlayerBalance = function() {
    return this.playerBalance;
};

Trade.prototype.setPlayerBalance = function(balance) {
    this.playerBalance = balance;
};

Trade.prototype.getWithPlayerBalance = function() {
    return this.withPlayerBalance;
};

Trade.prototype.setWithPlayerBalance = function(balance) {
    this.withPlayerBalance = balance;
};

Trade.prototype.isOffered = function() {
    return this.offered;
};

Trade.prototype.isAccepted = function() {
    return this.accepted;
};

Trade.prototype.makeOffer = function() {
    if (!this.canOffer()) {
        return;
    }

    this.offered = true;
};

Trade.prototype.canOffer = function() {
    if (!this.withPlayer) {
        return false;
    }

    if ((this.playerBalance == 0) && (!this.playerDeeds.length)) {
        return false;
    }

    if ((this.withPlayerBalance == 0) && (!this.withPlayerDeeds.length)) {
        return false;
    }

    return true;
};

Trade.prototype.accept = function() {
    if (!this.isOffered()) {
        return;
    }

    this.accepted = true;

    for (var key in this.playerDeeds) {
        this.player.getWallet().removeDeed(this.playerDeeds[key]);
        this.withPlayer.getWallet().addDeed(this.playerDeeds[key]);
    }

    for (var key in this.withPlayerDeeds) {
        this.withPlayer.getWallet().removeDeed(this.withPlayerDeeds[key]);
        this.player.getWallet().addDeed(this.withPlayerDeeds[key]);
    }

    this.player.getWallet().removeBalance(this.playerBalance);
    this.player.getWallet().addBalance(this.withPlayerBalance);

    this.withPlayer.getWallet().removeBalance(this.withPlayerBalance);
    this.withPlayer.getWallet().addBalance(this.playerBalance);
};

Trade.prototype.counter = function() {

    var trade = new Trade(this.withPlayer);

    trade.setWithPlayer(this.player);

    for (var key in this.playerDeeds) {
        trade.addWithPlayerDeed(this.playerDeeds[key]);
    }

    for (var key in this.withPlayerDeeds) {
        trade.addPlayerDeed(this.withPlayerDeeds[key]);
    }

    trade.setPlayerBalance(this.withPlayerBalance);
    trade.setWithPlayerBalance(this.playerBalance);

    return trade;
};

module.exports = Trade;
