var BoardConfig = function(config) {
    this.config = config;
};

BoardConfig.prototype.getDoubleRollsToJail = function() {
    return parseInt(this.config['double-rolls-to-jail']) || 3;
};

BoardConfig.prototype.getPlayerTimeout = function() {
    return parseInt(this.config['player-timeout']) || 180;
};

BoardConfig.prototype.getStartBalance = function() {
    return parseInt(this.config.amounts['start']) || 1500;
};

BoardConfig.prototype.getPassGoIncome = function() {
    return parseInt(this.config.amounts['pass-go']) || 200;
};

BoardConfig.prototype.getIncomeTax = function() {
    return parseInt(this.config.amounts['income-tax']) || 100;
};

BoardConfig.prototype.getSuperTax = function() {
    return parseInt(this.config.amounts['super-tax']) || 100;
};

BoardConfig.prototype.getOutOfJailFee = function() {
    return parseInt(this.config.amounts['get-out-jail']) || 50;
};

module.exports = BoardConfig;
