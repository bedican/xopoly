var Wallet = require('./wallet');
var Debt = require('./debt');
var idgen = require('./id-gen');

var Player = function(user, token, balance) {
    this.user = user;
    this.token = token;

    this.id = idgen.uniqueId();
    this.wallet = new Wallet(balance);
    this.debt = new Debt(this);
};

Player.prototype.export = function() {
    return {
        id: this.id,
        user: this.user.getId(),
        token: this.token.getName(),
        wallet: this.wallet.export(),
        debt: this.debt.export()
    };
};

Player.prototype.getId = function() {
    return this.id;
};

Player.prototype.getUser = function() {
    return this.user;
};

Player.prototype.getToken = function() {
    return this.token;
};

Player.prototype.getWallet = function() {
    return this.wallet;
};

Player.prototype.getDebt = function() {
    return this.debt;
};

module.exports = Player;
