module.exports = {
    'pass-go': function(board, state, player) {
        player.getWallet().addBalance(board.getConfig().getPassGoIncome());
    },
    'income-tax': function(board, state, player) {
        var amount = board.getConfig().getIncomeTax();

        if (player.getWallet().hasBalance(amount)) {
            player.getWallet().removeBalance(amount);
        } else {
            player.getDebt().addDebt(amount);
        }
    },
    'super-tax': function(board, state, player) {
        var amount = board.getConfig().getSuperTax();

        if (player.getWallet().hasBalance(amount)) {
            player.getWallet().removeBalance(amount);
        } else {
            player.getDebt().addDebt(amount);
        }
    },
    'deed': function(board, state, player) {
        var position = state.getPlayerPosition(player);
        var deed = board.getDeedDeck().getDeedByPosition(position);

        if (!deed) {
            return false;
        }

        var deedPlayer = board.getDeedDeck().getDeedOwner(state, deed);

        if ((deedPlayer) && (player.getId() != deedPlayer.getId()) && (!deed.isMortgaged())) {

            var rent = board.getDeedDeck().getDeedRent(state, deed);
            var wallet = player.getWallet();

            if (wallet.hasBalance(rent)) {
                player.getWallet().removeBalance(rent);
                deedPlayer.getWallet().addBalance(rent);
            } else {
                var debt = player.getDebt();

                debt.addDebt(rent);
                debt.addPlayerDebt(deedPlayer, rent);
            }
        }

        return deed;
    },
    'community-chest': function(board, state, player) {
        var card = board.getCardDeck().drawCard('community-chest');

        if (!card) {
            return false;
        }

        if (card.isRetainable()) {
            player.getWallet().addCard(card);
        } else {
            board.getCardDeck().addCard(card);
            card.doAction(board, state, player);
        }

        return card;
    },
    'chance': function(board, state, player) {
        var card = board.getCardDeck().drawCard('chance');

        if (!card) {
            return false;
        }

        if (card.isRetainable()) {
            player.getWallet().addCard(card);
        } else {
            board.getCardDeck().addCard(card);
            card.doAction(board, state, player);
        }

        return card;
    },
    'go-to-jail': function(board, state, player) {
        state.movePlayerToJail();
    }
};
