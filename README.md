# xopoly

This project provides an engine for playing a version of xopoly. Running it in isolation will work, however will only provide you with a development board, see `./boards/develop`.

To create a working application, with your own boards you will need the following. Boards can exist either within the container application itself or within their own modules.

- A container module/application.
- Boards module or modules, holding the board layouts and configurations.

A default application making use of a default board is provided with the modules [xopoly-app](https://www.npmjs.com/package/xopoly-app) and [xopoly-board-default](https://www.npmjs.com/package/xopoly-board-default).

## Installation

You can install the engine in isolation, and use the `develop` board, however if you wish to install a working game with default board, you should install the [xopoly-app](https://www.npmjs.com/package/xopoly-app) module.

``` bash
  $ npm install -g xopoly
```

## Creating an application

An application is a module that depends on the xopoly engine and optionally any board modules you wish to allow. The module [xopoly-app](https://www.npmjs.com/package/xopoly-app) provides an example of such an application.

To create your own application, we create the following files. Update the dependencies with the latest versions.

### `package.json`

```json
{
  "name": "xopoly-app-myapp",
  "version": "0.0.1",
  "description": "My xopoly app",
  "main": "lib/app.js",
  "bin": {
    "xopoly-app": "bin/app.js"
  },
  "dependencies": {
      "xopoly": "0.1.x",
      "xopoly-board-default": "0.1.x",
      "xopoly-board-myboard": "0.1.x"
  }
}
```

### `./bin/app.js`

```javascript
#! /usr/bin/env node

require('../lib/app').start();
```

### `./lib/app.js`

```javascript
var path = require('path');
var xopoly = require('xopoly');
var defaultBoards = require('xopoly-board-default');
var myBoards = require('xopoly-board-myboard');

module.exports = {
    start: function() {

        // Load application boards
        xopoly.addBoards(path.resolve(__dirname + '/../boards'));

        // Load default boards
        xopoly.addBoards(defaultBoards.getPath());

        // Load custom boards
        xopoly.addBoards(myBoards.getPath());

        // Begin
        xopoly.start();
    }
};
```

## Creating boards

Boards can either exist within an application itself, or within their own module, and are configured by providing a `boards` directory path which can contain one or more boards each within a subdirectory. The [xopoly-board-default](https://www.npmjs.com/package/xopoly-board-default) module provides an example of a board.

To create your own board as a module, we create the following files.

### `package.json`

```json
{
  "name": "xopoly-board-myboard",
  "version": "0.0.1",
  "description": "My xopoly board",
  "main": "lib/index.js",
  "files": [
    "boards",
    "lib"
  ]
}
```

### `./lib/index.js`

```javascript
var path = require('path');

module.exports = {
    getPath: function() {
        return path.resolve(__dirname + '/../boards');
    }
};
```

### The boards directory

The boards directory should contain one subdirectory for each board. To get started with a new board, you can create a copy of the `default` board from `xopoly-board-default` module.

Each board subdirectory should at minimum, contain the following.

- `./web` - This directory is mapped to the location `/boards/<board>` so image and css assets can be referenced.
- `./web/css/board.css` - The board css file, to override any default css from the engine.
- `./board.json` - Contains the configurtion needed for the board.

You should ensure all asset references within `board.json` or any css files reference the correct location for the board name, you should avoid cross referencing assets between boards.

## Development

The application is split in to two projects, the nodejs backend server, and reactjs frontend.

To run the backend server. The react build is available within the public directory, accessed at `http://localhost:3000`

```bash
$ ./bin/xopoly.js
```

To run a development server for react, (unknown requests are proxied to port 3000, the nodejs back end server). The development server is available at `http://localhost:3001`.

```bash
$ cd ./web-src
$ npm start
```

To build for production. This will place the compiled source in to the `./public` directory, which is the docroot for the backend server.

```bash
$ cd ./web-src
$ npm run-script build
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
